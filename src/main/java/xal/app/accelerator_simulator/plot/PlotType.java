/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.accelerator_simulator.plot;

/**
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public enum PlotType {
    TRAJECTORY("Trajectory", "mm"),
    ENVELOPE("Envelope", "mm"),
    LONGITUDINAL("Longitudinal", "mm"),
    ALPHA("Alpha", ""),
    BETA("Beta", "mm/pi mrad"),
    ENERGY("Energy", "eV"),
    MOMENTUM("Momentum", "eV/c"),
    SYNC_PHASE("Synchronous phase", "deg");

    private final String name;
    private final String units;

    private PlotType(String name, String units) {
        this.name = name;
        this.units = units;
    }

    public boolean equalsName(String otherName) {
        return name.equals(otherName);
    }

    @Override
    public String toString() {
        return name;
    }

    public static PlotType fromString(String text) {
        for (PlotType type : values()) {
            if (type.toString().equalsIgnoreCase(text)) {
                return type;
            }
        }
        return null;
    }

    public String getUnits() {
        return units;
    }
}

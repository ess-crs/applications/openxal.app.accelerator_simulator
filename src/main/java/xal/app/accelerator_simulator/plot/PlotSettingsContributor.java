/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.accelerator_simulator.plot;

import eu.ess.xaos.tools.annotation.BundleItem;
import eu.ess.xaos.tools.annotation.Bundles;
import eu.ess.xaos.tools.annotation.ServiceProvider;
import eu.ess.xaos.ui.plot.PluggableChartContainer;
import eu.ess.xaos.ui.plot.spi.ToolbarContributor;
import javafx.beans.binding.Bindings;
import javafx.scene.control.Control;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Tooltip;
import org.controlsfx.control.PopOver;
import static org.controlsfx.control.PopOver.ArrowLocation.TOP_CENTER;
import org.kordamp.ikonli.javafx.FontIcon;
import org.kordamp.ikonli.materialdesign.MaterialDesign;

/**
 * Chart toolbar contributor to select which elements to plot.
 * 
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
@ServiceProvider(service = ToolbarContributor.class, order = 900)
public class PlotSettingsContributor implements ToolbarContributor {

    @Override
    public boolean isPrecededBySeparator() {
        return true;
    }

    @Override
    @BundleItem(key = "button.tooltip", message = "Plot Settings")
    public Control provide(PluggableChartContainer chartContainer) {
        ToggleButton button = new ToggleButton(null, FontIcon.of(MaterialDesign.MDI_CHART_LINE, 14));

        button.setTooltip(new Tooltip(getString("button.tooltip")));
        button.setOnAction(e -> handleButton((PluggableChartContainerWrapper) chartContainer, button));
        button.disableProperty().bind(Bindings.or(
                Bindings.isNull(chartContainer.pluggableProperty()),
                button.selectedProperty()
        ));

        return button;

    }

    private String getString(String key, Object... parameters) {
        return Bundles.get(PlotSettingsContributor.class, key, parameters);
    }

    @BundleItem(key = "popOver.title", message = "Plot Settings")
    private void handleButton(PluggableChartContainerWrapper chartContainer, ToggleButton button) {
        PlotSettingsController controller = new PlotSettingsController(chartContainer);
        PopOver popOver = new PopOver(controller);

        popOver.setAnimated(false);
        popOver.setCloseButtonEnabled(true);
        popOver.setDetachable(true);
        popOver.setHeaderAlwaysVisible(true);
        popOver.setArrowLocation(TOP_CENTER);
        popOver.setOnShown(e -> popOver.getContentNode().requestFocus());
        popOver.setOnHidden(e -> {
            controller.dispose();
            button.setSelected(false);
        });
        popOver.setTitle(getString("popOver.title"));

        popOver.show(button);
    }
}

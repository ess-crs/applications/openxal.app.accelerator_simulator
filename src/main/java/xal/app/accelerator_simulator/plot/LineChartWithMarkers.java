/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.accelerator_simulator.plot;

import eu.ess.xaos.ui.plot.LineChartFX;
import java.util.Iterator;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.Axis;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.util.Pair;

/**
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class LineChartWithMarkers<X extends Object, Y extends Object> extends LineChartFX<X, Y> {

    private ObservableList<Data<X, X>> verticalRangeMarkers;

    // Minimum width for the markers.
    private static final double MIN_RECT_WIDTH = 3.;

    public LineChartWithMarkers(Axis<X> xAxis, Axis<Y> yAxis) {
        super(xAxis, yAxis);

        verticalRangeMarkers = FXCollections.observableArrayList(data -> new Observable[]{data.XValueProperty()});
        verticalRangeMarkers = FXCollections.observableArrayList(data -> new Observable[]{data.YValueProperty()});
        verticalRangeMarkers.addListener((InvalidationListener) observable -> plotMarkers());
    }

    public void addVerticalRangeMarker(Pair<X, X> marker) {
        Data<X, X> dataMarker = new Data<>(marker.getKey(), marker.getValue());

        Rectangle rectangle = new Rectangle(0, 0, 0, 0);
        rectangle.setStroke(Color.TRANSPARENT);
        rectangle.setFill(Color.BLUE.deriveColor(0, 1, 1, 0.2));

        dataMarker.setNode(rectangle);

        getPlotChildren().add(rectangle);
        verticalRangeMarkers.add(dataMarker);
    }

    public void removeAllVerticalRangeMarkers() {
        for (Data<X, X> dataMarker : verticalRangeMarkers) {
            if (dataMarker.getNode() != null) {
                getPlotChildren().remove(dataMarker.getNode());
                dataMarker.setNode(null);
            }
        }
        verticalRangeMarkers.clear();
    }

    public void removeVerticalRangeMarker(Pair<X, X> marker) {
        Iterator<Data<X, X>> it = verticalRangeMarkers.iterator();
        while (it.hasNext()) {
            Data<X, X> dataMarker = it.next();
            if (dataMarker.XValueProperty().getValue() == marker.getKey() && dataMarker.YValueProperty().getValue() == marker.getValue()) {
                if (dataMarker.getNode() != null) {
                    getPlotChildren().remove(dataMarker.getNode());
                    dataMarker.setNode(null);
                }
                it.remove();
            }
        }
    }

    protected void plotMarkers() {
        for (Data<X, X> verticalRangeMarker : verticalRangeMarkers) {
            Rectangle rectangle = (Rectangle) verticalRangeMarker.getNode();
            rectangle.setX(getXAxis().getDisplayPosition(verticalRangeMarker.getXValue()));
            double width = getXAxis().getDisplayPosition(verticalRangeMarker.getYValue());
            if (width < MIN_RECT_WIDTH) {
                width = MIN_RECT_WIDTH;
            }
            rectangle.setWidth(width);
            rectangle.setY(0.0);
            rectangle.setHeight(getBoundsInLocal().getHeight());
            rectangle.toBack();
        }
    }
}

/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.accelerator_simulator.plot;

import eu.ess.xaos.core.util.LogUtils;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.VBox;
import javafx.stage.WindowEvent;
import static javafx.stage.WindowEvent.WINDOW_CLOSE_REQUEST;

/**
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
class PlotSettingsController extends VBox implements Initializable {

    private static final Logger LOGGER = Logger.getLogger(PlotSettingsController.class.getName());

    private final PluggableChartContainerWrapper pluggableChartContainerWrapper;
    @FXML
    private CheckBox plotAperturesCB;
    @FXML
    private CheckBox plotDiagnosticsCB;
    @FXML
    private Button closeButton;
    @FXML
    private ComboBox<String> plotSelectorCB;

    public PlotSettingsController(PluggableChartContainerWrapper pluggableChartContainerWrapper) {
        this.pluggableChartContainerWrapper = pluggableChartContainerWrapper;

        init();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // init controller
        plotAperturesCB.setSelected(pluggableChartContainerWrapper.getDocument().isPlottingAperture());
        plotDiagnosticsCB.setSelected(pluggableChartContainerWrapper.getDocument().isPlottingDiagnostics());

        plotSelectorCB.getItems().setAll(Stream.of(PlotType.values())
                               .map(PlotType::toString)
                               .collect(Collectors.toList()));
        
        plotSelectorCB.getSelectionModel().select(pluggableChartContainerWrapper.plotType().get());
    }

    @FXML
    void close(ActionEvent event) {
        getScene().getWindow().fireEvent(new WindowEvent(getScene().getWindow(), WINDOW_CLOSE_REQUEST));
    }

    private void init() {
        URL resource = PlotSettingsController.class.getResource("/fxml/PlotSettings.fxml");

        try {
            FXMLLoader loader = new FXMLLoader(resource);

            loader.setController(this);
            loader.setRoot(this);
            loader.load();
        } catch (IOException ex) {
            LogUtils.log(LOGGER, Level.SEVERE, ex, "Unable to load ''PlotSettings.fxml'' resource [{0}].", resource.toExternalForm());
        }
    }

    protected void dispose() {
        // update plot        
        pluggableChartContainerWrapper.getDocument().plotApertureFlag().set(plotAperturesCB.isSelected());
        pluggableChartContainerWrapper.getDocument().plotDiagnosticsFlag().set(plotDiagnosticsCB.isSelected());
        pluggableChartContainerWrapper.plotType().set(plotSelectorCB.getSelectionModel().getSelectedItem());
    }
}

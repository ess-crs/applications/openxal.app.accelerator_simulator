/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.accelerator_simulator.plot;

import eu.ess.xaos.ui.plot.LineChartFX;
import eu.ess.xaos.ui.plot.NumberAxis;
import eu.ess.xaos.ui.plot.plugins.Plugins;
import eu.ess.xaos.ui.plot.util.SeriesColorUtils;
import eu.ess.xaos.ui.util.ColorUtils;
import eu.ess.xaos.ui.plot.util.LineStyle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.ValueAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.paint.Color;
import javafx.util.Pair;
import xal.app.accelerator_simulator.Simulation;

/**
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class ChartWrapper {

    private final LineChartWithMarkers<Number, Number> chart;

    private final ObservableList<XYChart.Series<Number, Number>> chartData = FXCollections.observableArrayList();

    private final ValueAxis<Number> xAxis = new NumberAxis();
    private final ValueAxis<Number> yAxis = new NumberAxis();
    private Color colorX;
    private Color colorY;
    private Color colorZ;
    private Color otherColor;
    private String suffix;
    private Map<String, Boolean> seriesDrawnInPlot = new HashMap<>();

    public ChartWrapper() {
        xAxis.setLabel("Position [m]");

        xAxis.setAnimated(false);
        yAxis.setAnimated(false);

        chart = new LineChartWithMarkers<>(xAxis, yAxis);

        chart.addChartPlugins(FXCollections.observableList(List.of(Plugins.all())));

        chart.setData(chartData);
        chart.setCreateSymbols(true);
        chart.setAnimated(false);
        chart.setAxisSortingPolicy(LineChart.SortingPolicy.NONE);
        chart.setLinesWidth(2);
    }

    public XYChart.Series<Number, Number> addDataSeries(String name, ObservableList<XYChart.Data<Number, Number>> data) {
        XYChart.Series<Number, Number> series = new XYChart.Series<>(name, FXCollections.observableArrayList());

        series.getData().addAll(data);

        // Restore the series drawn in plot flag if the series was plotted before refresh
        if (seriesDrawnInPlot.containsKey(name)) {
            chart.setSeriesDrawn(name, seriesDrawnInPlot.get(name));
        }
        
        chartData.add(series);

        return series;
    }

    public boolean removeDataSeries(XYChart.Series<Number, Number> data) {
        return chart.getData().remove(data);
    }

    public void clear() {
        seriesDrawnInPlot = chart.getData().stream().collect(Collectors.toMap(Series::getName, (series) -> chart.isSeriesDrawn(series.getName())));
        // Clearing only the data, the style should remain between updates to avoid glitches.
        chart.getData().clear();
    }

    public void setYAxisLabel(String label) {
        yAxis.setLabel(label);
    }

    public LineChartFX<Number, Number> getChart() {
        return chart;
    }

    public void addVerticalRangeMarker(Pair marker) {
        chart.addVerticalRangeMarker(marker);
    }

    public void removeVerticalRangeMarker(Pair marker) {
        chart.removeVerticalRangeMarker(marker);
    }

    public void removeAllVerticalRangeMarkers() {
        chart.removeAllVerticalRangeMarkers();
    }

    public void plot(PlotType plotSelection, Simulation sim) {
        plot(plotSelection, sim, false);
    }

    public void plot(PlotType plotSelection, Simulation sim, boolean reference) {
        suffix = reference ? " (Ref)" : "";
        colorX = reference ? ColorUtils.ESS_RED_DARK : SeriesColorUtils.HORIZONTAL;
        colorY = reference ? ColorUtils.ESS_PRIMARY_DARK : SeriesColorUtils.VERTICAL;
        colorZ = reference ? ColorUtils.ESS_GREEN_DARK : SeriesColorUtils.LONGITUDINAL;
        otherColor = reference ? ColorUtils.ESS_ORANGE_DARK : ColorUtils.ESS_ORANGE;

        switch (plotSelection) {
            case TRAJECTORY:
                plotTrajectory(sim);
                break;
            case ENVELOPE:
                plotEnvelope(sim);
                break;
            case LONGITUDINAL:
                plotLongitudinal(sim);
                break;
            case ALPHA:
                plotAlpha(sim);
                break;
            case BETA:
                plotBeta(sim);
                break;
            case ENERGY:
                plotKineticEnergy(sim);
                break;
            case MOMENTUM:
                plotMomentum(sim);
                break;
            case SYNC_PHASE:
                plotSynchronousPhase(sim);
                break;
            default:
                break;
        }
    }

    public void plotAperture(Simulation sim) {
        plotAperture(sim, false);
    }

    public void plotAperture(Simulation sim, boolean alsoNegative) {
        Series apXSeries = addDataSeries("Aperture x", sim.getProfileXData());
        Series apYSeries = addDataSeries("Aperture y", sim.getProfileYData());
        chart.setNotShownInLegend("Aperture x");
        chart.setNotShownInLegend("Aperture y");
        chart.setSeriesColor(apXSeries, Color.BLACK);
        chart.setSeriesColor(apYSeries, Color.BLACK);

        if (alsoNegative) {
            Series mApXSeries = addDataSeries("-Aperture x", sim.getProfile2XData());
            Series mApYSeries = addDataSeries("-Aperture y", sim.getProfile2YData());
            chart.setNotShownInLegend("-Aperture x");
            chart.setSeriesColor(mApXSeries, Color.BLACK);
            chart.setSeriesColor(mApYSeries, Color.BLACK);
            chart.setNotShownInLegend("-Aperture y");
        }
    }

    private void plotTrajectory(Simulation sim) {
        List<XYChart.Data<Number, Number>> meanXData = new ArrayList<>();
        List<XYChart.Data<Number, Number>> meanYData = new ArrayList<>();

        if (!sim.getPositions().isEmpty()) {
            for (int i = 0; i < sim.getPositions().size(); i++) {
                meanXData.add(new XYChart.Data<>(sim.getPositions().get(i), sim.getMeanx().get(i) * 1e3));
                meanYData.add(new XYChart.Data<>(sim.getPositions().get(i), sim.getMeany().get(i) * 1e3));
            }
        }

        Series meanXSeries = addDataSeries("Mean x" + suffix, FXCollections.observableList(meanXData));
        Series meanYSeries = addDataSeries("Mean y" + suffix, FXCollections.observableList(meanYData));

        chart.setSeriesColor(meanXSeries, colorX);
        chart.setSeriesColor(meanYSeries, colorY);
    }

    private void plotEnvelope(Simulation sim) {
        List<XYChart.Data<Number, Number>> sigmaXData = new ArrayList<>();
        List<XYChart.Data<Number, Number>> sigmaYData = new ArrayList<>();
        if (!sim.getPositions().isEmpty()) {
            for (int i = 0; i < sim.getPositions().size(); i++) {
                sigmaXData.add(new XYChart.Data<>(sim.getPositions().get(i), sim.getSigmax().get(i) * 1e3));
                sigmaYData.add(new XYChart.Data<>(sim.getPositions().get(i), sim.getSigmay().get(i) * 1e3));
            }
        }

        Series sigmaXSeries = addDataSeries("Sigma x" + suffix, FXCollections.observableList(sigmaXData));
        Series sigmaYSeries = addDataSeries("Sigma y" + suffix, FXCollections.observableList(sigmaYData));

        chart.setSeriesColor(sigmaXSeries, colorX);
        chart.setSeriesColor(sigmaYSeries, colorY);
    }

    private void plotLongitudinal(Simulation sim) {
        List<XYChart.Data<Number, Number>> meanZData = new ArrayList<>();
        List<XYChart.Data<Number, Number>> sigmaZData = new ArrayList<>();
        if (!sim.getPositions().isEmpty()) {
            for (int i = 0; i < sim.getPositions().size(); i++) {
                meanZData.add(new XYChart.Data<>(sim.getPositions().get(i), sim.getMeanz().get(i) * 1e3));
                sigmaZData.add(new XYChart.Data<>(sim.getPositions().get(i), sim.getSigmaz().get(i) * 1e3));
            }
        }

        Series meanZSeries = addDataSeries("Mean z" + suffix, FXCollections.observableList(meanZData));
        Series sigmaZSeries = addDataSeries("Sigma z" + suffix, FXCollections.observableList(sigmaZData));

        chart.setSeriesColor(meanZSeries, colorZ);
        chart.setSeriesColor(sigmaZSeries, colorZ);

        chart.setLineStyle(meanZSeries, LineStyle.DOTTED);
    }

    private void plotAlpha(Simulation sim) {
        List<XYChart.Data<Number, Number>> alphaxData = new ArrayList<>();
        List<XYChart.Data<Number, Number>> alphayData = new ArrayList<>();
        if (!sim.getPositions().isEmpty()) {
            for (int i = 0; i < sim.getPositions().size(); i++) {
                alphaxData.add(new XYChart.Data<>(sim.getPositions().get(i), sim.getAlphax().get(i) * 1e3));
                alphayData.add(new XYChart.Data<>(sim.getPositions().get(i), sim.getAlphay().get(i) * 1e3));
            }
        }

        Series alphaXSeries = addDataSeries("Alpha x" + suffix, FXCollections.observableList(alphaxData));
        Series alphaYSeries = addDataSeries("Alpha y" + suffix, FXCollections.observableList(alphayData));

        chart.setSeriesColor(alphaXSeries, colorX);
        chart.setSeriesColor(alphaYSeries, colorY);
    }

    private void plotBeta(Simulation sim) {
        List<XYChart.Data<Number, Number>> betaxData = new ArrayList<>();
        List<XYChart.Data<Number, Number>> betayData = new ArrayList<>();
        if (!sim.getPositions().isEmpty()) {
            for (int i = 0; i < sim.getPositions().size(); i++) {
                betaxData.add(new XYChart.Data<>(sim.getPositions().get(i), sim.getBetax().get(i) * 1e3));
                betayData.add(new XYChart.Data<>(sim.getPositions().get(i), sim.getBetay().get(i) * 1e3));
            }
        }

        Series betaXSeries = addDataSeries("Beta x" + suffix, FXCollections.observableList(betaxData));
        Series betaYSeries = addDataSeries("Beta y" + suffix, FXCollections.observableList(betayData));

        chart.setSeriesColor(betaXSeries, colorX);
        chart.setSeriesColor(betaYSeries, colorY);
    }

    private void plotKineticEnergy(Simulation sim) {
        List<XYChart.Data<Number, Number>> energyData = new ArrayList<>();
        if (!sim.getPositions().isEmpty()) {
            for (int i = 0; i < sim.getPositions().size(); i++) {
                energyData.add(new XYChart.Data<>(sim.getPositions().get(i), sim.getKineticEnergy().get(i) * 1e3));
            }
        }

        Series series = addDataSeries("Energy" + suffix, FXCollections.observableList(energyData));

        chart.setSeriesColor(series, otherColor);
    }

    private void plotMomentum(Simulation sim) {
        List<XYChart.Data<Number, Number>> momentumData = new ArrayList<>();
        if (!sim.getPositions().isEmpty()) {
            for (int i = 0; i < sim.getPositions().size(); i++) {
                momentumData.add(new XYChart.Data<>(sim.getPositions().get(i), sim.getMomentum().get(i) * 1e3));
            }
        }

        Series series = addDataSeries("Momentum" + suffix, FXCollections.observableList(momentumData));

        chart.setSeriesColor(series, otherColor);
    }

    private void plotSynchronousPhase(Simulation sim) {
        List<XYChart.Data<Number, Number>> synchronousPhaseData = new ArrayList<>();
        if (!sim.getPositions().isEmpty()) {
            for (int i = 0; i < sim.getSynchronousPhasePositions().size(); i++) {
                synchronousPhaseData.add(new XYChart.Data<>(sim.getSynchronousPhasePositions().get(i), sim.getSynchronousPhase().get(i)));
            }
        }

        Series series = addDataSeries("Synchronous Phase" + suffix, FXCollections.observableList(synchronousPhaseData));

        chart.setSeriesColor(series, colorZ);
        chart.setShowMarker(series, true);
        chart.setShowLine(series, false);
    }
}

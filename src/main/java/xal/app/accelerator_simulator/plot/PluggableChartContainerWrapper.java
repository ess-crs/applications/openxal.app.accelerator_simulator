/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.accelerator_simulator.plot;

import eu.ess.xaos.ui.plot.PluggableChartContainer;
import javafx.beans.property.StringProperty;
import xal.app.accelerator_simulator.AcceleratorSimDocument;
import xal.extension.fxapplication.XalFxDocument;

/**
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class PluggableChartContainerWrapper extends PluggableChartContainer {

    private final AcceleratorSimDocument document;
    private final StringProperty plotType;

    public PluggableChartContainerWrapper(XalFxDocument document, StringProperty plotType) {
        super();
        this.document = (AcceleratorSimDocument) document;
        this.plotType = plotType;
    }

    public AcceleratorSimDocument getDocument() {
        return document;
    }

    public StringProperty plotType() {
        return plotType;
    }
}

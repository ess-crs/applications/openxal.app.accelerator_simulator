/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.accelerator_simulator;

import java.net.MalformedURLException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.stage.Stage;
import xal.extension.fxapplication.FxApplication;

/**
 *
 * @author Juan F. Esteban Müller <juanf.estebanmuller@ess.eu>
 */
public class MainApp extends FxApplication {

    @Override
    public void setup(Stage stage) {
        // Set the main scene FXML file (in the resources directory)
        MAIN_SCENE = "/fxml/Scene.fxml";
        // Set the CSS style file (in the resources directory)
        CSS_STYLE = "/styles/Styles.css";
        // Set the application name for the title bar
        setApplicationName("Accelerator Simulator");
        // Set to false if this application doesn't save/load xml files
        HAS_DOCUMENTS = true;
        // Set to false if this application doesn't need the machine sequences
        HAS_SEQUENCE = true;
        // Setting the application document (remove if not used)
        DOCUMENT = new AcceleratorSimDocument(stage);
    }

    @Override
    public void beforeStart(Stage stage) {
        // Drag and drop hooks
        stage.getScene().setOnDragOver(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                if (event.getGestureSource() != stage.getScene()
                        && event.getDragboard().hasFiles()) {
                    event.acceptTransferModes(TransferMode.MOVE);
                }
                event.consume();
            }
        });

        stage.getScene().setOnDragDropped(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                Dragboard db = event.getDragboard();
                if (db.hasFiles()) {
                    if (db.getFiles().size() > 1) {
                        String title = "Trying to drag more than one file";
                        String msg = "Only dragging a single file is supported";
                        Logger.getLogger(MainApp.class.getName()).log(Level.FINE, "{0}.", msg);

                        AcceleratorSimDocument.showErrorDialog(title, msg);
                        event.setDropCompleted(false);
                        event.consume();
                        return;
                    }

                    // Check that the current document has no changes
                    if (DOCUMENT != null && DOCUMENT.hasChanges()) {
                        Alert dialog = new Alert(Alert.AlertType.CONFIRMATION);
                        dialog.setHeaderText("Document has unsaved changes, are you sure you want to load another document?");
                        dialog.setContentText("Unsaved changes in the current document will be lost.");
                        Optional<ButtonType> result = dialog.showAndWait();
                        if (!result.isPresent() || result.get() != ButtonType.OK) {
                            return;
                        }
                    }

                    try {
                        DOCUMENT.setSource(db.getFiles().get(0));
                        DOCUMENT.loadDocument(db.getFiles().get(0).toURI().toURL());
                        event.setDropCompleted(true);
                    } catch (MalformedURLException ex) {
                        event.setDropCompleted(false);
                        String msg = "Error loading dragged file";
                        Logger.getLogger(MainApp.class.getName()).log(Level.FINE, msg + ".", ex);
                        AcceleratorSimDocument.showErrorDialog(msg, "");
                    }
                }

                event.consume();
            }
        });
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}

/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.accelerator_simulator;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import xal.ca.BatchConnectionRequest;
import xal.ca.Channel;
import xal.ca.ChannelRecord;
import xal.ca.ConnectionListener;
import xal.ca.IEventSinkValue;
import xal.ca.Monitor;
import xal.ca.MonitorException;
import xal.smf.AcceleratorNode;
import xal.smf.AcceleratorSeq;
import xal.smf.AccessibleProperty;
import xal.smf.impl.RfCavity;

/**
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class LiveSimulationRunner extends AbstractMonitorsHandler implements IEventSinkValue, ConnectionListener {

    private final EventHandler<ActionEvent> stopHandler;

    LiveSimulationRunner(Simulation sim, EventHandler<ActionEvent> handler, EventHandler<ActionEvent> stopHandler) {
        super(sim, handler);
        this.stopHandler = stopHandler;
    }

    @Override
    public void initializeMonitors() {
        List<Channel> channels = new ArrayList<>();

        AcceleratorSeq sequence = sim.getSequence();
        for (AcceleratorNode node : sequence.getAllInclusiveNodes()) {
            // Ignore cavities in RF_DESIGN mode
            if (sim.getMode() != Simulation.Mode.RF_DESIGN || !node.isKindOf(RfCavity.TYPE)) {
                for (AccessibleProperty property : node.getAccessibleProperties()) {
                    if (property.hasDesignValues() && property.hasLiveValues()) {
                        String readbackHandle = property.getReadbackHandles()[0];
                        Channel readback = node.getChannel(readbackHandle);
                        channels.add(readback);
                    }
                }
            }
        }

        BatchConnectionRequest request = new BatchConnectionRequest(channels);
        request.submitAndWait(5.0);

        for (Channel readback : channels) {
            readback.addConnectionListener(this);
        }
    }

    @Override
    public void eventValue(ChannelRecord cr, Channel chnl) {
        notifyNewUpdate();
    }

    @Override
    protected void handle() {
        // Handler should run the simulation
        handler.handle(null);
        // Stop if there are no results
        if (!sim.hasResults()) {
            Platform.runLater(() -> stopHandler.handle(null));
        }
    }

    @Override
    public void connectionMade(Channel chnl) {
        try {
            if (chnl.isConnected()) {
                Monitor monitor = chnl.addMonitorValue(this, 0);
                monitors.add(monitor);
            }
        } catch (MonitorException ex) {
            Logger.getLogger(LiveSimulationRunner.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NullPointerException ex) {
            Logger.getLogger(LiveSimulationRunner.class.getName()).log(Level.WARNING, "Not possible to connect to channel {0}.", chnl.channelName());
            Logger.getLogger(LiveSimulationRunner.class.getName()).log(Level.FINE, "NullPointerException while connecting to channel " + chnl.channelName() + " ", ex);
            chnl.disconnect();
            chnl.requestConnection();
        }
    }

    @Override
    public void connectionDropped(Channel chnl) {
        Logger.getLogger(LiveSimulationRunner.class.getName()).log(Level.WARNING, "Channel {0} disconnected.", chnl.channelName());
    }
}

/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.accelerator_simulator;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.DoubleStream;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import xal.model.ModelException;
import xal.model.alg.EnvelopeTracker;
import xal.model.alg.SynchronousTracker;
import xal.model.elem.IdealRfCavity;
import xal.model.probe.EnvelopeProbe;
import xal.model.probe.SynchronousProbe;
import xal.model.probe.traj.EnvelopeProbeState;
import xal.model.probe.traj.SynchronousState;
import xal.sim.scenario.AlgorithmFactory;
import xal.sim.scenario.ProbeFactory;
import xal.sim.scenario.Scenario;
import xal.smf.Accelerator;
import xal.smf.AcceleratorNode;
import xal.smf.AcceleratorSeq;
import xal.smf.AcceleratorSeqCombo;
import xal.smf.AccessibleProperty;
import xal.smf.ApertureProfile;
import xal.smf.proxy.PrimaryPropertyAccessor;
import xal.smf.proxy.PropertyAccessor;
import xal.tools.beam.CovarianceMatrix;
import xal.tools.beam.PhaseIndex;
import xal.tools.beam.PhaseVector;
import xal.tools.beam.Twiss;
import xal.tools.data.DataAdaptor;

/**
 * This class handles data related to a simulation.
 *
 * TODO: show and save initial parameters used in simulation (apart from current parameters)
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class Simulation {

    private static final Logger LOGGER = Logger.getLogger(Simulation.class.getName());

    private static final SimpleDateFormat DF = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
    private static final PrimaryPropertyAccessor propertyAccessor = new PrimaryPropertyAccessor();

    private Date timestamp;
    private AcceleratorSeq sequence;

    // Properties to show in the TableView
    private StringProperty timestampString = new SimpleStringProperty("");
    private StringProperty sequenceString = new SimpleStringProperty("");
    private IntegerProperty modeInt = new SimpleIntegerProperty(Mode.DESIGN.getValue());
    private StringProperty comment = new SimpleStringProperty("");
    private BooleanProperty referenceFlag = new SimpleBooleanProperty(false);

    // Store of the initial parameters of the simulation
    private InitialParameters initialParameters = new InitialParameters();

    // Store of the accelerator settings
    private Map<String, Map<String, Double>> currentAttributes = new TreeMap<>();

    // Store of the accelerator settings used for the last simulation
    private Map<String, Map<String, Double>> simAttributes = new TreeMap<>();

    // Results from the simulation
    private List<Double> positions = new ArrayList<>();
    // Centroid
    private List<Double> meanx = new ArrayList<>();
    private List<Double> meany = new ArrayList<>();
    private List<Double> meanz = new ArrayList<>();

    // Envelope
    private List<Double> sigmax = new ArrayList<>();
    private List<Double> sigmay = new ArrayList<>();
    private List<Double> sigmaz = new ArrayList<>();

    // Twiss
    private List<Double> alphax = new ArrayList<>();
    private List<Double> alphay = new ArrayList<>();
    private List<Double> betax = new ArrayList<>();
    private List<Double> betay = new ArrayList<>();

    // Energy
    private List<Double> energy = new ArrayList<>();
    private List<Double> momentum = new ArrayList<>();

    // Synchronous phase
    private List<Double> synchronousPhasePositions = new ArrayList<>();
    private List<Double> synchronousPhase = new ArrayList<>();

    // flag that indicates if the simulation was successfully run
    private boolean resultsFlag = false;

    private EnvelopeTracker envelopeTracker;
    private EnvelopeProbe probe;

    private List<SimulationListener> listeners = new ArrayList<>();

    private boolean plotted = false;
    private LiveSimulationRunner liveSimulationRunner;
    private double[] profilePos;
    private double[] profileX;
    private double[] profile2X;
    private double[] profileY;
    private double[] profile2Y;
    private SynchronousTracker synchronousTracker;
    private SynchronousProbe synchronousProbe;

    public void startLiveSimulation(LiveSimulationRunner liveSimulationRunner) {
        this.liveSimulationRunner = liveSimulationRunner;
        liveSimulationRunner.start();
    }

    public void stopLiveSimulation() {
        if (liveSimulationRunner != null) {
            liveSimulationRunner.stop();
            liveSimulationRunner = null;
        }
    }

    // Simulation Mode
    public enum Mode {
        DESIGN(0), LIVE(1), RF_DESIGN(2);

        private final int value;

        Mode(int newValue) {
            value = newValue;
        }

        public int getValue() {
            return value;
        }
    }

    public Simulation() {
    }

    public Simulation(AcceleratorSeq sequence) {
        setSequence(sequence);
        // Save the current state of the simulation.
        storeCurrentAttributes();
    }

    protected Map<String, Map<String, Double>> getSimAttributes() {
        return simAttributes;
    }

    public void addSimulationListener(SimulationListener listener) {
        listeners.add(listener);
    }

    public void removeSimulationListener(SimulationListener listener) {
        listeners.remove(listener);
    }

    public void clearSimulationListeners() {
        listeners.clear();
    }

    public boolean isPlotted() {
        return plotted;
    }

    public void setPlotted(boolean plotted) {
        this.plotted = plotted;
    }

    public void write(DataAdaptor mainAdaptor) {
        DataAdaptor simAdaptor = mainAdaptor.createChild("simulation");
        // Store simulation details        
        simAdaptor.setValue("timestamp", getTimestamp());
        simAdaptor.setValue("sequence", getSequence());
        simAdaptor.setValue("mode", getMode().name());
        simAdaptor.setValue("comment", getComment());
        simAdaptor.setValue("reference", getReferenceFlag());
        simAdaptor.setValue("accelerator", getSequence().getAccelerator().getSystemId());
        simAdaptor.setValue("accelerator_version", getSequence().getAccelerator().getVersion());

        // Store initial Parameters
        getInitialParameters().write(simAdaptor);

        // Store accelerator design values
        DataAdaptor currentAttributesAdaptor = simAdaptor.createChild("currentAttributes");
        for (Map.Entry<String, Map<String, Double>> node : currentAttributes.entrySet()) {
            DataAdaptor nodeAdaptor = currentAttributesAdaptor.createChild(node.getKey());
            for (Map.Entry<String, Double> value : node.getValue().entrySet()) {
                nodeAdaptor.setValue(value.getKey(), value.getValue());
            }
        }

        DataAdaptor simAttributesAdaptor = simAdaptor.createChild("simAttributes");
        for (Map.Entry<String, Map<String, Double>> node : simAttributes.entrySet()) {
            DataAdaptor nodeAdaptor = simAttributesAdaptor.createChild(node.getKey());
            for (Map.Entry<String, Double> value : node.getValue().entrySet()) {
                nodeAdaptor.setValue(value.getKey(), value.getValue());
            }
        }

        // Store simulation results   
        if (resultsFlag) {
            DataAdaptor resultsAdaptor = simAdaptor.createChild("results");
            DataAdaptor positionsAdaptor = resultsAdaptor.createChild("positions");
            positionsAdaptor.setValue("value", positions.stream().mapToDouble(d -> d).toArray());
            // Centroid
            DataAdaptor meanxAdaptor = resultsAdaptor.createChild("meanx");
            meanxAdaptor.setValue("value", meanx.stream().mapToDouble(d -> d).toArray());
            DataAdaptor meanyAdaptor = resultsAdaptor.createChild("meany");
            meanyAdaptor.setValue("value", meany.stream().mapToDouble(d -> d).toArray());
            DataAdaptor meanzAdaptor = resultsAdaptor.createChild("meanz");
            // Envelope
            meanzAdaptor.setValue("value", meanz.stream().mapToDouble(d -> d).toArray());
            DataAdaptor sigmaxAdaptor = resultsAdaptor.createChild("sigmax");
            sigmaxAdaptor.setValue("value", sigmax.stream().mapToDouble(d -> d).toArray());
            DataAdaptor sigmayAdaptor = resultsAdaptor.createChild("sigmay");
            sigmayAdaptor.setValue("value", sigmay.stream().mapToDouble(d -> d).toArray());
            DataAdaptor sigmazAdaptor = resultsAdaptor.createChild("sigmaz");
            sigmazAdaptor.setValue("value", sigmaz.stream().mapToDouble(d -> d).toArray());
            // Twiss
            DataAdaptor alphaxAdaptor = resultsAdaptor.createChild("alphax");
            alphaxAdaptor.setValue("value", alphax.stream().mapToDouble(d -> d).toArray());
            DataAdaptor alphayAdaptor = resultsAdaptor.createChild("alphay");
            alphayAdaptor.setValue("value", alphay.stream().mapToDouble(d -> d).toArray());
            DataAdaptor betaxAdaptor = resultsAdaptor.createChild("betax");
            betaxAdaptor.setValue("value", betax.stream().mapToDouble(d -> d).toArray());
            DataAdaptor betayAdaptor = resultsAdaptor.createChild("betay");
            betayAdaptor.setValue("value", betay.stream().mapToDouble(d -> d).toArray());
            // Energy
            DataAdaptor energyAdaptor = resultsAdaptor.createChild("energy");
            energyAdaptor.setValue("value", energy.stream().mapToDouble(d -> d).toArray());
            // Momentum
            DataAdaptor momentumAdaptor = resultsAdaptor.createChild("momentum");
            momentumAdaptor.setValue("value", momentum.stream().mapToDouble(d -> d).toArray());
            // Synchronous phase
            DataAdaptor synchronousPhaseAdaptor = resultsAdaptor.createChild("synchronousPhase");
            synchronousPhaseAdaptor.setValue("value", synchronousPhase.stream().mapToDouble(d -> d).toArray());
            DataAdaptor synchronousPhasePositionsAdaptor = resultsAdaptor.createChild("synchronousPhasePositions");
            synchronousPhasePositionsAdaptor.setValue("value", synchronousPhasePositions.stream().mapToDouble(d -> d).toArray());
        }
    }

    public void load(DataAdaptor simAdaptor, Accelerator accelerator) throws LoadingFileException {
        // Load simulation details     
        if (!simAdaptor.hasAttribute("timestamp") || !simAdaptor.hasAttribute("mode")
                || !simAdaptor.hasAttribute("comment") || !simAdaptor.hasAttribute("reference")
                || !simAdaptor.hasAttribute("sequence")) {
            throw new LoadingFileException();
        }

        timestampString.setValue(simAdaptor.stringValue("timestamp"));
        setMode(simAdaptor.stringValue("mode"));
        setComment(simAdaptor.stringValue("comment"));
        setReferenceFlag(simAdaptor.booleanValue("reference"));

        String sequenceName = simAdaptor.stringValue("sequence");
        if ("Full".equals(sequenceName)) {
            setSequence(accelerator);
        } else {
            setSequence(accelerator.getSequence(sequenceName));
        }

        // Load initial Parameters
        getInitialParameters().load(simAdaptor);

        // Load accelerator design values
        DataAdaptor currentAttributesAdaptor = simAdaptor.childAdaptor("currentAttributes");
        if (currentAttributesAdaptor == null) {
            throw new LoadingFileException();
        }

        for (DataAdaptor nodeAdaptor : currentAttributesAdaptor.childAdaptors()) {
            Map<String, Double> atts = new TreeMap<>();
            for (String attribute : nodeAdaptor.attributes()) {
                atts.put(attribute, nodeAdaptor.doubleValue(attribute));
            }
            currentAttributes.put(nodeAdaptor.name(), atts);
        }

        DataAdaptor simAttributesAdaptor = simAdaptor.childAdaptor("simAttributes");
        if (simAttributesAdaptor != null) {
            for (DataAdaptor nodeAdaptor : simAttributesAdaptor.childAdaptors()) {
                Map<String, Double> atts = new TreeMap<>();
                for (String attribute : nodeAdaptor.attributes()) {
                    atts.put(attribute, nodeAdaptor.doubleValue(attribute));
                }
                simAttributes.put(nodeAdaptor.name(), atts);
            }
        }

        try {
            loadNodeAttributes();
        } catch (LoadingAttributesException ex) {
            throw new LoadingFileException(ex);
        }

        // Load simulation results
        DataAdaptor resultsAdaptor = simAdaptor.childAdaptor("results");
        resultsFlag = resultsAdaptor != null;
        if (resultsFlag) {
            if (resultsAdaptor.childAdaptor("positions") == null || resultsAdaptor.childAdaptor("meanx") == null
                    || resultsAdaptor.childAdaptor("meany") == null || resultsAdaptor.childAdaptor("meanz") == null
                    || resultsAdaptor.childAdaptor("sigmax") == null || resultsAdaptor.childAdaptor("sigmay") == null
                    || resultsAdaptor.childAdaptor("sigmaz") == null
                    || resultsAdaptor.childAdaptor("alphax") == null || resultsAdaptor.childAdaptor("alphay") == null
                    || resultsAdaptor.childAdaptor("betax") == null || resultsAdaptor.childAdaptor("betay") == null
                    || resultsAdaptor.childAdaptor("energy") == null || resultsAdaptor.childAdaptor("momentum") == null
                    || resultsAdaptor.childAdaptor("synchronousPhasePositions") == null || resultsAdaptor.childAdaptor("synchronousPhase") == null) {
                throw new LoadingFileException();
            }

            DoubleStream.of(resultsAdaptor.childAdaptor("positions").doubleArray("value")).boxed().forEach(d -> positions.add(d));
            DoubleStream.of(resultsAdaptor.childAdaptor("meanx").doubleArray("value")).boxed().forEach(d -> meanx.add(d));
            DoubleStream.of(resultsAdaptor.childAdaptor("meany").doubleArray("value")).boxed().forEach(d -> meany.add(d));
            DoubleStream.of(resultsAdaptor.childAdaptor("meanz").doubleArray("value")).boxed().forEach(d -> meanz.add(d));
            DoubleStream.of(resultsAdaptor.childAdaptor("sigmax").doubleArray("value")).boxed().forEach(d -> sigmax.add(d));
            DoubleStream.of(resultsAdaptor.childAdaptor("sigmay").doubleArray("value")).boxed().forEach(d -> sigmay.add(d));
            DoubleStream.of(resultsAdaptor.childAdaptor("sigmaz").doubleArray("value")).boxed().forEach(d -> sigmaz.add(d));
            DoubleStream.of(resultsAdaptor.childAdaptor("alphax").doubleArray("value")).boxed().forEach(d -> alphax.add(d));
            DoubleStream.of(resultsAdaptor.childAdaptor("alphay").doubleArray("value")).boxed().forEach(d -> alphay.add(d));
            DoubleStream.of(resultsAdaptor.childAdaptor("betax").doubleArray("value")).boxed().forEach(d -> betax.add(d));
            DoubleStream.of(resultsAdaptor.childAdaptor("betay").doubleArray("value")).boxed().forEach(d -> betay.add(d));
            DoubleStream.of(resultsAdaptor.childAdaptor("energy").doubleArray("value")).boxed().forEach(d -> energy.add(d));
            DoubleStream.of(resultsAdaptor.childAdaptor("momentum").doubleArray("value")).boxed().forEach(d -> momentum.add(d));
            DoubleStream.of(resultsAdaptor.childAdaptor("synchronousPhasePositions").doubleArray("value")).boxed().forEach(d -> synchronousPhasePositions.add(d));
            DoubleStream.of(resultsAdaptor.childAdaptor("synchronousPhase").doubleArray("value")).boxed().forEach(d -> synchronousPhase.add(d));
        }
    }

    /**
     * Save the accelerator parameters used for the current simulation. The attributes are obtained from the Scenario
     * object, and it therefore takes into account the synchronization mode (DESIGN, RF_DESIGN, or LIVE).
     *
     * @param scenario
     */
    public final void storeSimAttributes(Scenario scenario) {
        simAttributes.clear();

        boolean updatedAttributes = false;

        for (AcceleratorNode node : sequence.getAllInclusiveNodes()) {
            Map<String, Double> atts = new TreeMap<>();

            for (AccessibleProperty property : node.getAccessibleProperties()) {
                if (property.hasDesignValues()) {
                    Map<String, Double> propertiesForNode = scenario.propertiesForNode(node);
                    // Using Reflections to get the conversion factor for the given property
                    PropertyAccessor nodeAccessor = propertyAccessor.getAccessorFor(node);
                    Class propertyAccessorClass = nodeAccessor.getClass();
                    double conversionFactor = 1;
                    try {
                        Method retrieveItems = propertyAccessorClass.getDeclaredMethod("getPropertyScale", String.class);
                        retrieveItems.setAccessible(true);
                        conversionFactor = (double) retrieveItems.invoke(nodeAccessor, property.getName());
                    } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                        Logger.getLogger(Simulation.class.getName()).log(Level.FINEST, "getPropertyScale not found for propertyAccessorClass = {0}", propertyAccessorClass);
                    }
                    atts.put(property.getName(), propertiesForNode.get(property.getName()) / conversionFactor);
                }
            }
            if (!atts.isEmpty()) {
                simAttributes.put(node.getId(), atts);
                updatedAttributes = true;
            }
        }

        // Inform listeners
        if (updatedAttributes) {
            for (SimulationListener listener : listeners) {
                listener.updated();
            }
        }
    }

    /**
     * This method takes the attributes from all nodes in the sequence.
     */
    public final void storeCurrentAttributes() {
        currentAttributes.clear();
        for (AcceleratorNode node : sequence.getAllInclusiveNodes()) {
            Map<String, Double> atts = new TreeMap<>();
            for (AccessibleProperty property : node.getAccessibleProperties()) {
                if (property.hasDesignValues()) {
                    atts.put(property.getName(), property.getDesign());
                }
            }
            if (!atts.isEmpty()) {
                currentAttributes.put(node.getId(), atts);
            }
        }
    }

    /**
     * This method loads the attributes for all nodes in the sequence.
     */
    public void loadNodeAttributes() throws LoadingAttributesException {
        for (AcceleratorNode node : sequence.getAllInclusiveNodes()) {

            Map<String, Double> atts = currentAttributes.get(node.getId());
            for (AccessibleProperty property : node.getAccessibleProperties()) {
                if (property.hasDesignValues()) {
                    if (atts == null) {
                        throw new LoadingAttributesException();
                    }
                    if (!atts.containsKey(property.getName())) {
                        throw new LoadingAttributesException();
                    }
                    property.setDesign(atts.get(property.getName()));
                }
            }
        }
    }

    public AcceleratorSeq getSequence() {
        return sequence;
    }

    public final void setSequence(AcceleratorSeq sequence) {
        this.sequence = sequence;

        AcceleratorSeq seq = sequence;
        if (seq instanceof Accelerator) {
            seq = new AcceleratorSeqCombo("Full", seq.getSequences());
        }
        sequenceString.set(seq.getId());

        try {
            envelopeTracker = AlgorithmFactory.createEnvelopeTracker(seq);
            probe = ProbeFactory.getEnvelopeProbe(seq, envelopeTracker);

            synchronousTracker = AlgorithmFactory.createSynchronousTracker(sequence);
            synchronousProbe = ProbeFactory.getSynchronousProbe(sequence, synchronousTracker);

            // Load initial parameters
            initialParameters.setCurrent(probe.getBeamCurrent() * 1e3);
            initialParameters.setEnergy(probe.getKineticEnergy() * 1e-6);

            PhaseVector centroid = probe.getCovariance().getMean();
            initialParameters.setCentroid(centroid.getArrayCopy());
            Twiss[] twiss = probe.getCovariance().computeTwiss();
            initialParameters.setAlpha(new double[]{twiss[0].getAlpha(), twiss[1].getAlpha(), twiss[2].getAlpha()});
            initialParameters.setBeta(new double[]{twiss[0].getBeta(), twiss[1].getBeta(), twiss[2].getBeta()});
            initialParameters.setEmitt(new double[]{twiss[0].getEmittance(), twiss[1].getEmittance(), twiss[2].getEmittance()});
        } catch (InstantiationException ex) {
            LOGGER.log(Level.SEVERE, "Error while creating the envelope tracker.", ex);
        }

        loadAperture();
    }

    public void setSequence(String newSeq) throws SimulationException {
        if (sequence.getAccelerator().getSequence(newSeq) != null) {
            setSequence(sequence.getAccelerator().getSequence(newSeq));
        } else if (sequence.getAccelerator().getComboSequence(newSeq) != null) {
            setSequence(sequence.getAccelerator().getComboSequence(newSeq));
        } else if (newSeq.equals("")) {
            setSequence(sequence.getAccelerator());
        } else {
            throw new SimulationException("Sequence " + newSeq + " not found.");
        }

        loadAperture();
    }

    public void clearResults() {
        // Clear the results flag so that the plot does not show data for the previously selected sequence.
        resultsFlag = false;
    }

    private void loadAperture() {
        ApertureProfile aperture = sequence.getAperProfile();

        List<Double> profilePosL = aperture.getProfilePos();
        List<Double> profileXL = aperture.getProfileX();
        List<Double> profileYL = aperture.getProfileY();

        profilePos = new double[profilePosL.size()];
        profileX = new double[profilePosL.size()];
        profile2X = new double[profilePosL.size()];
        profileY = new double[profilePosL.size()];
        profile2Y = new double[profilePosL.size()];

        for (int i = 0; i < profilePosL.size(); i++) {
            profilePos[i] = profilePosL.get(i);
            profileX[i] = profileXL.get(i);
            profileY[i] = profileYL.get(i);
        }

        for (int i = 0; i < profilePosL.size(); i++) {
            profile2X[i] = -profileXL.get(i);
            profile2Y[i] = -profileYL.get(i);
        }
    }

    protected double[] getProfilePos() {
        return profilePos;
    }

    protected double[] getProfileX() {
        return profileX;
    }

    protected double[] getProfileY() {
        return profileY;
    }

    protected double[] getProfile2X() {
        return profile2X;
    }

    protected double[] getProfile2Y() {
        return profile2Y;
    }

    public ObservableList<XYChart.Data<Number, Number>> getProfileXData() {
        ObservableList<XYChart.Data<Number, Number>> profileXData = FXCollections.observableArrayList();
        for (int i = 0; i < profilePos.length; i++) {
            profileXData.add(new XYChart.Data<>(profilePos[i], profileX[i] * 1e3));
        }
        return profileXData;
    }

    public ObservableList<XYChart.Data<Number, Number>> getProfileYData() {
        ObservableList<XYChart.Data<Number, Number>> profileYData = FXCollections.observableArrayList();
        for (int i = 0; i < profilePos.length; i++) {
            profileYData.add(new XYChart.Data<>(profilePos[i], profileY[i] * 1e3));
        }
        return profileYData;
    }

    public ObservableList<XYChart.Data<Number, Number>> getProfile2XData() {
        ObservableList<XYChart.Data<Number, Number>> profile2XData = FXCollections.observableArrayList();

        for (int i = 0; i < profilePos.length; i++) {
            profile2XData.add(new XYChart.Data<>(profilePos[i], -profileX[i] * 1e3));

        }
        return profile2XData;
    }

    public ObservableList<XYChart.Data<Number, Number>> getProfile2YData() {
        ObservableList<XYChart.Data<Number, Number>> profile2YData = FXCollections.observableArrayList();
        for (int i = 0; i < profilePos.length; i++) {
            profile2YData.add(new XYChart.Data<>(profilePos[i], -profileY[i] * 1e3));
        }
        return profile2YData;
    }

    InitialParameters getInitialParameters() {
        return initialParameters;
    }

    public StringProperty timestampStringProperty() {
        return timestampString;
    }

    public StringProperty sequenceStringProperty() {
        return sequenceString;
    }

    public IntegerProperty modeIntProperty() {
        return modeInt;
    }

    public StringProperty commentProperty() {
        return comment;
    }

    public BooleanProperty referenceFlagProperty() {
        return referenceFlag;
    }

    public String getTimestamp() {
        return timestampString.getValue();
    }

    public Mode getMode() {
        for (Mode mode : Mode.values()) {
            if (mode.getValue() == getModeInt()) {
                return mode;
            }
        }
        return null;
    }

    public void setMode(String mode) {
        modeInt.set(Mode.valueOf(mode).getValue());
    }

    public int getModeInt() {
        return modeInt.get();
    }

    public String getComment() {
        return comment.get();
    }

    public void setComment(String comment) {
        this.comment.set(comment);
    }

    public boolean getReferenceFlag() {
        return referenceFlag.get();
    }

    public void setReferenceFlag(boolean flag) {
        referenceFlag.set(flag);
    }

    public List<Double> getPositions() {
        return positions;
    }

    public List<Double> getMeanx() {
        return meanx;
    }

    public List<Double> getMeany() {
        return meany;
    }

    public List<Double> getMeanz() {
        return meanz;
    }

    public List<Double> getSigmax() {
        return sigmax;
    }

    public List<Double> getSigmay() {
        return sigmay;
    }

    public List<Double> getSigmaz() {
        return sigmaz;
    }

    public List<Double> getAlphax() {
        return alphax;
    }

    public List<Double> getAlphay() {
        return alphay;
    }

    public List<Double> getBetax() {
        return betax;
    }

    public List<Double> getBetay() {
        return betay;
    }

    public List<Double> getKineticEnergy() {
        return energy;
    }

    public List<Double> getMomentum() {
        return momentum;

    }

    public List<Double> getSynchronousPhasePositions() {
        return synchronousPhasePositions;
    }

    public List<Double> getSynchronousPhase() {
        return synchronousPhase;
    }

    public boolean hasResults() {
        return resultsFlag;
    }

    /**
     * Run a simulation with the current initial parameters and settings.
     */
    public void run() {
        // Load initial parameters
        probe.reset();
        probe.setBeamCurrent(initialParameters.getCurrent() * 1e-3);
        probe.setKineticEnergy(initialParameters.getEnergy() * 1e6);

        PhaseVector vec = new PhaseVector();
        Twiss[] twissIni = new Twiss[3];
        double[] alphaIni = initialParameters.getAlpha();
        double[] betaIni = initialParameters.getBeta();
        double[] emitt = initialParameters.getEmitt();
        for (int i = 0; i < 3; i++) {
            vec.setElem(PhaseIndex.spatialIndices().toArray(new PhaseIndex[0])[i], initialParameters.getCentroid()[i]);
            twissIni[i] = new Twiss(alphaIni[i], betaIni[i], emitt[i]);
        }

        CovarianceMatrix covMat = CovarianceMatrix.buildCovariance(twissIni[0],
                twissIni[1], twissIni[2], vec);
        probe.setCovariance(covMat);

        AcceleratorSeq seq = sequence;
        if (seq instanceof Accelerator) {
            seq = new AcceleratorSeqCombo("Full", seq.getSequences());
        }

        try {
            Scenario model = Scenario.newScenarioFor(seq);
            model.setProbe(probe);

            switch (getMode()) {
                case DESIGN:
                    model.setSynchronizationMode(Scenario.SYNC_MODE_DESIGN);
                    break;
                case RF_DESIGN:
                    model.setSynchronizationMode(Scenario.SYNC_MODE_RF_DESIGN);
                    break;
                default:
                    model.setSynchronizationMode(Scenario.SYNC_MODE_LIVE);
                    break;
            }

            model.resync();

            model.run();

            EnvelopeProbe resultProbe = (EnvelopeProbe) model.getProbe();

            positions.clear();
            meanx.clear();
            meany.clear();
            meanz.clear();
            sigmax.clear();
            sigmay.clear();
            sigmaz.clear();
            alphax.clear();
            betax.clear();
            alphay.clear();
            betay.clear();
            energy.clear();
            momentum.clear();

            for (EnvelopeProbeState state : resultProbe.getTrajectory().getStatesViaIndexer()) {
                positions.add(state.getPosition());
                meanx.add(state.getCovarianceMatrix().getMeanX());
                meany.add(state.getCovarianceMatrix().getMeanY());
                meanz.add(state.getCovarianceMatrix().getMeanZ());
                sigmax.add(state.getCovarianceMatrix().getSigmaX());
                sigmay.add(state.getCovarianceMatrix().getSigmaY());
                sigmaz.add(state.getCovarianceMatrix().getSigmaZ());
                Twiss twiss[] = state.getCovarianceMatrix().computeTwiss();
                alphax.add(twiss[0].getAlpha());
                betax.add(twiss[0].getBeta());
                alphay.add(twiss[1].getAlpha());
                betax.add(twiss[0].getBeta());
                betay.add(twiss[1].getBeta());
                energy.add(state.getKineticEnergy());
                momentum.add(state.getMomentum());
            }

            synchronousPhasePositions.clear();
            synchronousPhase.clear();

            model.setProbe(synchronousProbe);

            model.resync();
            model.run();

            synchronousProbe = (SynchronousProbe) model.getProbe();

            for (SynchronousState state : synchronousProbe.getTrajectory().getStatesViaIndexer()) {
                if (state.getElementTypeId().equals(IdealRfCavity.STR_TYPEID)) {
                    synchronousPhasePositions.add(state.getPosition());
                    synchronousPhase.add(state.getSynchronousPhase() * 180. / Math.PI);
                }
            }

            timestamp = Date.from(Instant.now());
            timestampString.set(DF.format(timestamp));

            resultsFlag = true;

            // Save the accelerator settings used for this simulation.
            storeSimAttributes(model);
        } catch (IllegalArgumentException | ModelException ex) {
            resultsFlag = false;
            String msg = "Error while running the model";
            LOGGER.log(Level.FINE, msg + ".", ex);

            Platform.runLater(() -> {
                Alert alert = new Alert(AlertType.ERROR);
                alert.setTitle(msg);
                alert.setHeaderText(msg);
                alert.setContentText(ex.getMessage());

                // Create expandable Exception.
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                ex.printStackTrace(pw);
                String exceptionText = sw.toString();

                Label label = new Label("The exception stacktrace was:");

                TextArea textArea = new TextArea(exceptionText);
                textArea.setEditable(false);
                textArea.setWrapText(true);

                textArea.setMaxWidth(Double.MAX_VALUE);
                textArea.setMaxHeight(Double.MAX_VALUE);
                GridPane.setVgrow(textArea, Priority.ALWAYS);
                GridPane.setHgrow(textArea, Priority.ALWAYS);

                GridPane expContent = new GridPane();
                expContent.setMaxWidth(Double.MAX_VALUE);
                expContent.add(label, 0, 0);
                expContent.add(textArea, 0, 1);

                // Set expandable Exception into the dialog pane.
                alert.getDialogPane().setExpandableContent(expContent);

                alert.showAndWait();
            });
        }
    }
}

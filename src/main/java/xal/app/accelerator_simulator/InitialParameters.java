/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.accelerator_simulator;

import java.util.ArrayList;
import java.util.List;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import xal.tools.data.DataAdaptor;

/**
 * Class to hold initial parameters for simulations.
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
class InitialParameters implements ChangeListener {

    private final ObservableList<PropertyWrapper> generalProperties = FXCollections.observableArrayList();
    private final PropertyWrapper current = new PropertyWrapper("Current [mA]", Double.toString(0.0));
    private final PropertyWrapper energy = new PropertyWrapper("Energy [MeV]", Double.toString(0.0));

    private final ObservableList<ThreeDimWrapper> threeDimProperties = FXCollections.observableArrayList();
    private ThreeDimWrapper centroid = new ThreeDimWrapper("Centroid", new double[3]);
    private ThreeDimWrapper alpha = new ThreeDimWrapper("Alpha", new double[3]);
    private ThreeDimWrapper beta = new ThreeDimWrapper("Beta", new double[3]);
    private ThreeDimWrapper emitt = new ThreeDimWrapper("Emittance", new double[3]);

    private List<EventHandler<ActionEvent>> handlers = new ArrayList<>();

    public InitialParameters() {
        generalProperties.add(current);
        generalProperties.add(energy);

        threeDimProperties.add(centroid);
        threeDimProperties.add(alpha);
        threeDimProperties.add(beta);
        threeDimProperties.add(emitt);

        current.valueProperty().addListener(this);
        energy.valueProperty().addListener(this);

        centroid.valueXProperty().addListener(this);
        centroid.valueYProperty().addListener(this);
        centroid.valueZProperty().addListener(this);

        alpha.valueXProperty().addListener(this);
        alpha.valueYProperty().addListener(this);
        alpha.valueZProperty().addListener(this);

        beta.valueXProperty().addListener(this);
        beta.valueYProperty().addListener(this);
        beta.valueZProperty().addListener(this);

        emitt.valueXProperty().addListener(this);
        emitt.valueYProperty().addListener(this);
        emitt.valueZProperty().addListener(this);
    }

    public ObservableList<PropertyWrapper> getGeneralProperties() {
        return generalProperties;
    }

    public ObservableList<ThreeDimWrapper> getThreeDimProperties() {
        return threeDimProperties;
    }

    public double getCurrent() {
        return Double.parseDouble((String) current.getValue());
    }

    public void setCurrent(double current) {
        this.current.setValue(Double.toString(current));
    }

    public double getEnergy() {
        return Double.parseDouble((String) energy.getValue());
    }

    public void setEnergy(double energy) {
        this.energy.setValue(Double.toString(energy));
    }

    public double[] getCentroid() {
        return centroid.getValue();
    }

    public void setCentroid(double[] centroid) {
        this.centroid.setValue(centroid);
    }

    public double[] getAlpha() {
        return alpha.getValue();
    }

    public void setAlpha(double[] alpha) {
        this.alpha.setValue(alpha);
    }

    public double[] getBeta() {
        return beta.getValue();
    }

    public void setBeta(double[] beta) {
        this.beta.setValue(beta);
    }

    public double[] getEmitt() {
        return emitt.getValue();
    }

    public void setEmitt(double[] emitt) {
        this.emitt.setValue(emitt);
    }

    public void write(DataAdaptor simAdaptor) {
        DataAdaptor iniParamsAdaptor = simAdaptor.createChild("initial_parameters");

        iniParamsAdaptor.setValue("current", current.getValue());
        iniParamsAdaptor.setValue("energy", energy.getValue());

        iniParamsAdaptor.setValue("centroid", centroid.getValue());
        iniParamsAdaptor.setValue("alpha", alpha.getValue());
        iniParamsAdaptor.setValue("beta", beta.getValue());
        iniParamsAdaptor.setValue("emitt", emitt.getValue());
    }

    public void load(DataAdaptor simAdaptor) throws LoadingFileException {
        DataAdaptor iniParamsAdaptor = simAdaptor.childAdaptor("initial_parameters");
        if (iniParamsAdaptor == null) {
            throw new LoadingFileException();
        }
        if (!iniParamsAdaptor.hasAttribute("current") || !iniParamsAdaptor.hasAttribute("energy")
                || !iniParamsAdaptor.hasAttribute("centroid") || !iniParamsAdaptor.hasAttribute("alpha")
                || !iniParamsAdaptor.hasAttribute("beta") || !iniParamsAdaptor.hasAttribute("emitt")) {
            throw new LoadingFileException();
        }

        setCurrent(iniParamsAdaptor.doubleValue("current"));
        setEnergy(iniParamsAdaptor.doubleValue("energy"));
        setCentroid(iniParamsAdaptor.doubleArray("centroid"));
        setAlpha(iniParamsAdaptor.doubleArray("alpha"));
        setBeta(iniParamsAdaptor.doubleArray("beta"));
        setEmitt(iniParamsAdaptor.doubleArray("emitt"));
    }

    protected void addEventHandler(EventHandler<ActionEvent> listener) {
        handlers.add(listener);
    }

    protected void remveEventHandler(EventHandler<ActionEvent> listener) {
        handlers.remove(listener);
    }

    protected void clearEventHandler() {
        handlers.clear();
    }

    @Override
    public void changed(ObservableValue ov, Object t, Object t1) {
        for (EventHandler<ActionEvent> handler : handlers) {
            handler.handle(null);
        }
    }
}

/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.accelerator_simulator;

import xal.app.accelerator_simulator.plot.ChartWrapper;
import eu.ess.xaos.ui.plot.plugins.Pluggable;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.Phaser;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TitledPane;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeItem;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.Pair;
import xal.app.accelerator_simulator.plot.PlotType;
import xal.app.accelerator_simulator.plot.PluggableChartContainerWrapper;
import xal.extension.fxapplication.Controller;
import xal.extension.fxapplication.widgets.AcceleratorTreeView;
import xal.extension.fxapplication.widgets.ComboSequencesTreeView;
import xal.extension.jels.smf.impl.FieldMapFactory;
import xal.extension.jels.smf.impl.RfFieldMap1D;
import xal.smf.Accelerator;
import xal.smf.AcceleratorNode;
import xal.smf.AcceleratorSeq;

/**
 * FXML Controller class
 *
 * @author Juan F. Esteban Müller <juanf.estebanmuller@ess.eu>
 */
public class FXMLController extends Controller {

    private AcceleratorTreeView modelTreeView;
    private ComboSequencesTreeView comboTreeView;

    boolean areAllFiltersSelected;
    String[] selectedFilters;

    private PluggableChartContainerWrapper upperChartContainer;
    private PluggableChartContainerWrapper bottomChartContainer;

    private ChartWrapper upperPlot = new ChartWrapper();
    private ChartWrapper bottomPlot = new ChartWrapper();

    private DiagnosticsMonitor diagnosticsMonitor;

    private Phaser diagnosticsPhaser = new Phaser(0) {
        /**
         * This is to use the same Phase the whole time, never terminating it.
         */
        @Override
        protected boolean onAdvance(int phase, int registeredParties) {
            return false;
        }
    };

    @FXML
    private GridPane treePane;
    @FXML
    private VBox simulatorVBox;
    @FXML
    private TableView<?> propertiesTV;
    @FXML
    private TableView<?> epicsTV;
    @FXML
    private TitledPane propertiesTP;
    @FXML
    private TableColumn<?, ?> tcProperty;
    @FXML
    private TableColumn<?, ?> tcValue;
    @FXML
    private TableColumn<?, ?> tcName;
    @FXML
    private TableColumn<?, ?> tcLiveValue;
    @FXML
    private SplitPane latticeSP;
    @FXML
    private GridPane centerPane;
    @FXML
    private TableColumn<?, ?> tcDesignValue;
    @FXML
    private GridPane accTreePane;
    @FXML
    private GridPane comboTreePane;
    @FXML
    private TableColumn<Simulation, String> timestampTC;
    @FXML
    private TableColumn<Simulation, Object> modeTC;
    @FXML
    private TableColumn<?, ?> commentTC;
    @FXML
    private TableColumn<Simulation, Object> runTC;
    @FXML
    private Button removeB;
    @FXML
    private Button addB;
    @FXML
    private TitledPane attributesTP;
    @FXML
    private TableView<?> attributesTV;
    @FXML
    private TitledPane epicsTP;
    @FXML
    private TableColumn<?, ?> tcHandle;
    @FXML
    private TableColumn<?, ?> tcEpicsValue;
    @FXML
    private TableView<Simulation> simTV;
    @FXML
    private TableView<PropertyWrapper> paramTV;
    @FXML
    private TableColumn<?, ?> paramTC;
    @FXML
    private TableColumn<PropertyWrapper, String> paramValueTC;
    @FXML
    private TableView<ThreeDimWrapper> paramCoordTV;
    @FXML
    private TableColumn<ThreeDimWrapper, String> paramCoordTC;
    @FXML
    private TableColumn<ThreeDimWrapper, String> xTC;
    @FXML
    private TableColumn<ThreeDimWrapper, String> yTC;
    @FXML
    private TableColumn<ThreeDimWrapper, String> zTC;
    @FXML
    private TableColumn<?, ?> sequenceTC;
    @FXML
    private TableColumn<Simulation, Boolean> referenceTC;
    @FXML
    private TableColumn<?, ?> tcSimValue;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Setting up the AcceleratorTreeView
        modelTreeView = new AcceleratorTreeView();
        accTreePane.add(modelTreeView, 0, 0);

        modelTreeView.getTreeView().getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        // Setting up the AcceleratorTreeView
        comboTreeView = new ComboSequencesTreeView();
        comboTreePane.add(comboTreeView, 0, 0);
        comboTreeView.enableDefaultClickEventHandler();

        // If a node is selected on a TreeView, remove selection from the other treeview.
        modelTreeView.addSingleClickEventHandler(e -> {
            comboTreeView.clearSelection();
            TreeItem<AcceleratorNode> selectedItem = modelTreeView.getSelectionModel().getSelectedItem();
            selectedElementListener(selectedItem);
        });
        comboTreeView.addSingleClickEventHandler(e -> {
            modelTreeView.clearSelection();
            TreeItem<AcceleratorNode> selectedItem = comboTreeView.getSelectionModel().getSelectedItem();
            selectedElementListener(selectedItem);
        });
    }

    @Override
    public void beforeStart() {
        modelTreeView.setDocument(getApplication().getDocument());

        areAllFiltersSelected = modelTreeView.areAllFiltersSelected();
        selectedFilters = modelTreeView.getSelectedFilters();

        comboTreeView.setDocument(getApplication().getDocument());

        // Plots
        upperChartContainer = new PluggableChartContainerWrapper(getApplication().getDocument(), ((AcceleratorSimDocument) getApplication().getDocument()).upperPlotType());
        bottomChartContainer = new PluggableChartContainerWrapper(getApplication().getDocument(), ((AcceleratorSimDocument) getApplication().getDocument()).bottomPlotType());

        // Adding the upper plot
        upperChartContainer.setPluggable((Pluggable) upperPlot.getChart());
        simulatorVBox.getChildren().add(upperChartContainer);

        // Adding the bottom plot
        bottomChartContainer.setPluggable((Pluggable) bottomPlot.getChart());
        simulatorVBox.getChildren().add(bottomChartContainer);

        // Bind the initial parameters tab
        simulationParametersTableViewSetup();

        // Bind simulation panel with document
        simulationTableViewSetup();

        // Reducing the number of points for RF Fieldmaps
        FieldMapFactory.getInstances()
                .values().stream().filter(
                        fieldMap -> (fieldMap instanceof RfFieldMap1D)).forEachOrdered(
                        fieldMap -> ((RfFieldMap1D) fieldMap).setNumberOfPoints(200));

        getDocument().getAcceleratorProperty().addChangeListener((ChangeListener<Accelerator>) (ov, t, t1) -> acceleratorChangeHandler(ov, t, t1));
        getDocument().getSequenceProperty().addListener((ChangeListener<String>) (ov, t, t1) -> sequenceChangeHandler(ov, t, t1));

        getDocument().plotDiagnosticsFlag().addListener((ChangeListener<Boolean>) (ov, t, t1) -> updatePlot());
        getDocument().plotApertureFlag().addListener((ChangeListener<Boolean>) (ov, t, t1) -> updatePlot());
        getDocument().bottomPlotType().addListener((ChangeListener<String>) (ov, t, t1) -> updatePlot());
        getDocument().upperPlotType().addListener((ChangeListener<String>) (ov, t, t1) -> updatePlot());
    }

    private void acceleratorChangeHandler(ObservableValue<? extends Accelerator> ov, Accelerator prevAcc, Accelerator newAcc) {
        final Simulation sim = simTV.getSelectionModel().getSelectedItem();

        if (sim != null) {
            // A simulation is selected, then use it to load the sequence.
            String seqId = sim.getSequence().getId();
            AcceleratorSeq seq = newAcc.getSequence(seqId);
            // Chaging the accelerator for the currently selected simulation
            if (sim.getSequence().getAccelerator() != newAcc) {
                if (seq == null) {
                    seq = newAcc.getComboSequence(seqId);
                }
                if (seq == null) {
                    seq = newAcc;
                }
                sim.setSequence(seq);
            }
            // This will trigger a new plot and reload of diagnostics
            // Using platform.runLater to make sure this runs after all accelerator change listener hooks.
            Platform.runLater(() -> getDocument().setSequence(sim.getSequence().getId()));
        } else {
            // No simulations, sequence is set to null by FxApplication. 
            // Plot should already be empty.
            // Clearing diagnostics.
            updateDiagnostics(null);
            // Clearing selection to empty right panels.
            selectedElementListener(null);
        }
    }

    private void sequenceChangeHandler(ObservableValue<? extends String> ov, String prevSeq, String newSeq) {
        Simulation sim = simTV.getSelectionModel().getSelectedItem();
        if (sim != null) {
            try {
                if (!sim.getSequence().getId().equals(newSeq)) {
                    // If switching sequence for the currently selected simulation, then clear results.
                    sim.clearResults();
                    // When the accelerator changes for the currently selected simulation, this method is called twice. 
                    // Ignore when it is called by FxApplication setting the sequence to null.
                    if (newSeq != null) {
                        sim.setSequence(newSeq);
                    }
                }
            } catch (SimulationException ex) {
                Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        // Stop plotting diagnostics for the previous sequence and start for the new one
        updateDiagnostics(sim);

        // Clear the plots
        clearPlots();

        selectedElementListener(null);
    }

    private void simulationListChangeHandler(ListChangeListener.Change<? extends Simulation> c) {
        while (c.next()) {
            if (c.wasAdded()) {
                Simulation addedSim = c.getAddedSubList().get(0);
                // Select the new item.
                Platform.runLater(() -> simTV.getSelectionModel().select(addedSim));
            }
        }
    }

    private void updateDiagnostics(Simulation sim) {
        if (!getDocument().isPlottingDiagnostics()) {
            return;
        }
        diagnosticsPhaser.register();

        Runnable updateDiagnosticsRunnable = () -> {
            if (diagnosticsMonitor != null) {
                diagnosticsMonitor.stop();
            }

            if (sim != null) {
                diagnosticsMonitor = new DiagnosticsMonitor(sim, (e) -> {
                    // Only plot if flag is enabled and simulation results are also plotted.
                    if (getDocument().isPlottingDiagnostics() && sim.hasResults()) {
                        updatePlot();
                    }
                });
                diagnosticsMonitor.start();
            }

            diagnosticsPhaser.arriveAndDeregister();
        };

        Thread updateDiagnosticsThread = new Thread(updateDiagnosticsRunnable);
        updateDiagnosticsThread.start();
    }

    /**
     * When a simulation is selected, changes the accelerator and sequence if
     * they are different from the currently selected simulation. It also loads
     * the settings used for the simulation, both the initial parameters and
     * machine settings.
     *
     * @param ov
     * @param prevSim
     * @param newSim
     */
    private void simulationChangeHandler(ObservableValue<? extends Simulation> ov, Simulation prevSim, Simulation newSim) {
        if (prevSim != null) {
            prevSim.storeCurrentAttributes();
            prevSim.clearSimulationListeners();
            // Set plotted to true to make possible to update the plot if mode is LIVE.
            prevSim.setPlotted(prevSim.getReferenceFlag());
        }

        if (newSim != null) {
            try {
                newSim.loadNodeAttributes();
                getDocument().setSelectedSimulation(newSim);
            } catch (LoadingAttributesException ex) {
                Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        removeB.setDisable(newSim == null);

        if (newSim != null) {
            String prevSeq = getDocument().getSequence();
            String newSeq = null;
            if (newSim.getSequence() != null) {
                newSeq = newSim.getSequence().getId();
            }

            AcceleratorNode prevElement = modelTreeView.getSelectedNode();
            AcceleratorNode prevCombo = comboTreeView.getSelectedNode();

            Accelerator prevAcc = getDocument().getAccelerator();
            Accelerator newAcc = newSim.getSequence().getAccelerator();

            // If accelerator is different, load it.
            if (prevAcc != newAcc) {
                // This will also update the sequence
                getDocument().setAccelerator(newAcc);
            } // If sequence is different, load it
            else if (prevSeq == null && newSeq != null || !prevSeq.equals(newSeq)) {
                getDocument().setSequence(newSeq);
            } else {
                updateDiagnostics(newSim);
            }

            // Selecting the same element in the treeviews.
            if (prevAcc != newAcc || prevSeq == null || !prevSeq.equals(newSeq)) {
                if (prevElement != null) {
                    modelTreeView.selectElement(prevElement.getId());
                } else if (prevCombo != null) {
                    comboTreeView.selectElement(prevCombo.getId());
                } else {
                    selectedElementListener(null);
                }
            }

            // Forcing reloading of right panels
            if (prevElement != null) {
                selectedElementListener(modelTreeView.findElement(prevElement.getId()));
            } else if (prevCombo != null) {
                selectedElementListener(comboTreeView.findElement(prevCombo.getId()));
            }

            // Load initial parameters
            paramTV.itemsProperty().setValue(newSim.getInitialParameters().getGeneralProperties());
            paramCoordTV.itemsProperty().setValue(newSim.getInitialParameters().getThreeDimProperties());

            // Set plotted to true to make possible to update the plot if mode is LIVE.
            newSim.setPlotted(true);
        } else {
            updateDiagnostics(newSim);
        }

        // change plot. Using Platform.runLater to make sure it runs after all accelerator/sequence change hooks
        updatePlot();
    }

    private void simulationParametersTableViewSetup() {
        paramTC.setCellValueFactory(new PropertyValueFactory<>("name"));
        paramValueTC.setCellFactory(TextFieldTableCell.forTableColumn());
        paramValueTC.setCellValueFactory(new PropertyValueFactory<>("value"));
        paramTV.setEditable(true);
        paramTV.getSelectionModel().cellSelectionEnabledProperty().set(true);

        paramCoordTC.setCellValueFactory(new PropertyValueFactory<>("name"));
        xTC.setCellValueFactory(new PropertyValueFactory<>("valueX"));
        yTC.setCellValueFactory(new PropertyValueFactory<>("valueY"));
        zTC.setCellValueFactory(new PropertyValueFactory<>("valueZ"));

        xTC.setCellFactory(TextFieldTableCell.forTableColumn());
        yTC.setCellFactory(TextFieldTableCell.forTableColumn());
        zTC.setCellFactory(TextFieldTableCell.forTableColumn());

        paramCoordTV.setEditable(true);
        paramCoordTV.getSelectionModel().cellSelectionEnabledProperty().set(true);
    }

    private static void addIconAndToolTipToButton(Button button, String style, String tooltipMessage) {
        Region icon = new Region();
        icon.getStyleClass().add(style);
        icon.setPrefHeight(16);
        icon.setPrefWidth(16);

        Tooltip tooltip = new Tooltip(tooltipMessage);
        button.setGraphic(icon);
        button.setTooltip(tooltip);
    }

    private void simulationTableViewSetup() {
        simTV.setEditable(true);

        getDocument().getSimulations().addListener((ListChangeListener) c -> simulationListChangeHandler(c));

        simTV.getSelectionModel().selectedItemProperty().addListener((ov, t, t1) -> simulationChangeHandler(ov, t, t1));

        timestampTC.setCellFactory(NonEditableCell.forTableColumn());

        timestampTC.setCellValueFactory(new PropertyValueFactory<>("timestampString"));
        modeTC.setCellValueFactory(new PropertyValueFactory<>("modeInt"));
        runTC.setCellValueFactory(new PropertyValueFactory<>("modeInt"));
        commentTC.setCellValueFactory(new PropertyValueFactory<>("comment"));
        sequenceTC.setCellValueFactory(new PropertyValueFactory<>("sequenceString"));
        referenceTC.setCellValueFactory(new PropertyValueFactory<>("referenceFlag"));

        simTV.itemsProperty().setValue(getDocument().getSimulations());

        Callback<TableColumn<Simulation, Object>, TableCell<Simulation, Object>> clbckModeTC = new Callback<>() {

            @Override
            public TableCell<Simulation, Object> call(TableColumn<Simulation, Object> column) {
                return new TableCell<Simulation, Object>() {

                    private ComboBox<String> combobox;

                    @Override
                    public void updateItem(Object item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            combobox = new ComboBox(FXCollections.observableArrayList(Simulation.Mode.DESIGN.name(), Simulation.Mode.RF_DESIGN.name(), Simulation.Mode.LIVE.name()));
                            setGraphic(combobox);
                            Simulation sim = getTableView().getItems().get(getIndex());
                            combobox.getSelectionModel().select(sim.getMode().name());
                            combobox.getSelectionModel().selectedItemProperty().addListener((ov, t, t1) -> sim.setMode(t1));
                        }
                    }
                };
            }
        };

        modeTC.setCellFactory(clbckModeTC);

        Callback<TableColumn<Simulation, Object>, TableCell<Simulation, Object>> clbckRunTC = new Callback<>() {

            @Override
            public TableCell<Simulation, Object> call(TableColumn<Simulation, Object> column) {
                return new TableCell<Simulation, Object>() {

                    private final HBox buttonBox = new HBox();

                    private final Button runDesignButton = new Button();
                    private final Button runLiveButton = new Button();
                    private final Button runLiveOnceButton = new Button();
                    private final Button runRfDesignButton = new Button();
                    private final Button runRfDesignOnceButton = new Button();
                    private final Button pauseLiveButton = new Button();

                    {
                        buttonBox.setAlignment(Pos.CENTER);
                        buttonBox.setSpacing(5);

                        addIconAndToolTipToButton(runDesignButton, "button-play", "Run in DESIGN mode");
                        addIconAndToolTipToButton(runLiveButton, "button-play", "Run continuouusly in LIVE mode");
                        addIconAndToolTipToButton(runLiveOnceButton, "button-play-one", "Run one simulation in LIVE mode");
                        addIconAndToolTipToButton(runRfDesignButton, "button-play", "Run continuouusly in RF_DESIGN mode");
                        addIconAndToolTipToButton(runRfDesignOnceButton, "button-play-one", "Run one simulation in RF_DESIGN mode");
                        addIconAndToolTipToButton(pauseLiveButton, "button-pause", "Pause simulations");

                        EventHandler<ActionEvent> runOnce = t -> {
                            Simulation sim = getTableView().getItems().get(getIndex());
                            sim.run();
                            updatePlot();
                        };

                        EventHandler<ActionEvent> runContinuously = t -> {
                            Simulation sim = getTableView().getItems().get(getIndex());

                            sim.getInitialParameters().clearEventHandler();
                            sim.getInitialParameters().addEventHandler(runOnce);

                            buttonBox.getChildren().clear();
                            buttonBox.getChildren().add(pauseLiveButton);
                            pauseLiveButton.setOnAction(e -> {
                                // Clear event handlers on initial parameter changes
                                sim.getInitialParameters().clearEventHandler();

                                // Stop runner
                                sim.stopLiveSimulation();

                                buttonBox.getChildren().clear();
                                buttonBox.getChildren().add(runLiveButton);
                                buttonBox.getChildren().add(runLiveOnceButton);
                            });

                            // Start simulations            
                            LiveSimulationRunner liveSimulationRunner = new LiveSimulationRunner(sim, runOnce, pauseLiveButton.getOnAction());
                            sim.startLiveSimulation(liveSimulationRunner);
                        };

                        runDesignButton.setOnAction(t -> {
                            runDesignButton.setDisable(true);
                            Thread runOnceThread = new Thread(() -> {
                                runOnce.handle(null);
                                runDesignButton.setDisable(false);
                            });
                            runOnceThread.start();
                        });
                        runLiveOnceButton.setOnAction(t -> {
                            runLiveButton.setDisable(true);
                            runLiveOnceButton.setDisable(true);
                            Thread runLiveOnceThread = new Thread(() -> {
                                runOnce.handle(null);
                                runLiveButton.setDisable(false);
                                runLiveOnceButton.setDisable(false);
                            });
                            runLiveOnceThread.start();
                        });
                        runRfDesignOnceButton.setOnAction(t -> {
                            runRfDesignOnceButton.setDisable(true);
                            Thread runRfDesignOnceThread = new Thread(() -> {
                                runOnce.handle(null);
                                runRfDesignOnceButton.setDisable(false);
                            });
                            runRfDesignOnceThread.start();
                        });
                        runLiveButton.setOnAction(runContinuously);
                        runRfDesignButton.setOnAction(runContinuously);
                    }

                    @Override
                    public void updateItem(Object item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            Simulation sim = getTableView().getItems().get(getIndex());
                            buttonBox.getChildren().clear();
                            if (sim.getMode() == Simulation.Mode.DESIGN) {
                                buttonBox.getChildren().add(runDesignButton);
                            } else {
                                buttonBox.getChildren().add(runLiveButton);
                                buttonBox.getChildren().add(runLiveOnceButton);
                            }
                            setGraphic(buttonBox);
                        }
                    }
                };
            }
        };
        runTC.setCellFactory(clbckRunTC);

        // Reference flag column
        Callback<TableColumn<Simulation, Boolean>, TableCell<Simulation, Boolean>> clbckRefTC = new Callback<>() {
            @Override
            public TableCell<Simulation, Boolean> call(TableColumn<Simulation, Boolean> column) {
                return new TableCell<Simulation, Boolean>() {

                    private final CheckBox referenceFlagCB = new CheckBox();

                    {
                        ChangeListener<Boolean> cl = new ChangeListener<>() {
                            @Override
                            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                                // When selecting the first elements, this method is called twice, the first time with index -1
                                if (getIndex() == -1) {
                                    return;
                                }
                                Simulation selectedSim = getTableRow().getItem();
                                if (selectedSim == null) {
                                    return;
                                }
                                if (t1) {
                                    selectedSim.setPlotted(true);

                                    for (Simulation sim : getDocument().getSimulations()) {
                                        if (sim != selectedSim && sim.getReferenceFlag()) {
                                            sim.setReferenceFlag(false);
                                            // If there was another checkbox selected, return here to skip plotting. 
                                            // The plot will be done when this method is called by the checkbox that gets disabled.
                                            return;
                                        }
                                    }
                                } else {
                                    // Update plotted flag depending on whether the checkbox corresponds to the selected
                                    // simulation or not
                                    selectedSim.setPlotted(selectedSim == getDocument().getSelectedSimulation());
                                }
                                plot();
                            }
                        };
                        referenceFlagCB.selectedProperty().addListener(cl);
                    }

                    @Override
                    public void updateItem(Boolean item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(referenceFlagCB);
                            Simulation sim = getTableRow().getItem();
                            if (sim != null) {
                                referenceFlagCB.selectedProperty().bindBidirectional(sim.referenceFlagProperty());
                            }
                        }
                    }
                };
            }
        };
        referenceTC.setCellFactory(clbckRefTC);
    }

    private void clearPlots() {
        upperPlot.clear();
        bottomPlot.clear();
    }

    // update plot after document has changes using Platform.runLater() to make sure it is executed in the JavaFX thread
    private void updatePlot() {
        Platform.runLater(() -> {
            plot();
            getDocument().setHasChanges(true);
        });
    }

    public void plot() {
        clearPlots();

        String upperPlotYAxisLabel = getDocument().getUpperPlotType().toString();
        if (!getDocument().getUpperPlotType().getUnits().equals("")) {
            upperPlotYAxisLabel += " [" + getDocument().getUpperPlotType().getUnits() + "]";
        }
        String bottomPlotYAxisLabel = getDocument().getBottomPlotType().toString();
        if (!getDocument().getBottomPlotType().getUnits().equals("")) {
            bottomPlotYAxisLabel += " [" + getDocument().getBottomPlotType().getUnits() + "]";
        }
        upperPlot.setYAxisLabel(upperPlotYAxisLabel);
        bottomPlot.setYAxisLabel(bottomPlotYAxisLabel);

        Simulation simulation = simTV.getSelectionModel().getSelectedItem();

        Simulation referenceSimulation = null;
        for (Simulation sim : getDocument().getSimulations()) {
            if (sim.getReferenceFlag()) {
                referenceSimulation = sim;
            }
        }

        if (getDocument().isPlottingAperture()) {
            // Only plot aperture if there are simulation results (reference or current).
            if (simulation != null && simulation.hasResults()) {
                upperPlot.plotAperture(simulation, getDocument().getUpperPlotType() == PlotType.TRAJECTORY);
                bottomPlot.plotAperture(simulation, getDocument().getBottomPlotType() == PlotType.TRAJECTORY);
            } else if (referenceSimulation != null && referenceSimulation.hasResults()) {
                upperPlot.plotAperture(referenceSimulation, getDocument().getUpperPlotType() == PlotType.TRAJECTORY);
                bottomPlot.plotAperture(referenceSimulation, getDocument().getBottomPlotType() == PlotType.TRAJECTORY);
            }
        }

        // plot reference simulation if enabled
        if (referenceSimulation != null && referenceSimulation.hasResults()) {
            upperPlot.plot(getDocument().getUpperPlotType(), referenceSimulation, true);
            bottomPlot.plot(getDocument().getBottomPlotType(), referenceSimulation, true);
        }

        // Plot simulated data if there are simulation results.
        if (simulation != null && simulation.hasResults()) {
            upperPlot.plot(getDocument().getUpperPlotType(), simulation);
            bottomPlot.plot(getDocument().getBottomPlotType(), simulation);

            if (getDocument().isPlottingDiagnostics()) {
                if (getDocument().getUpperPlotType() == PlotType.TRAJECTORY) {
                    // Only update the diagnostics monitor plot if all task related to diagnostics monitor are done
                    if (diagnosticsPhaser.getRegisteredParties() == 0) {
                        diagnosticsMonitor.plot(upperPlot);
                    }
                }
                if (getDocument().getBottomPlotType() == PlotType.TRAJECTORY) {
                    // Only update the diagnostics monitor plot if all task related to diagnostics monitor are done
                    if (diagnosticsPhaser.getRegisteredParties() == 0) {
                        diagnosticsMonitor.plot(bottomPlot);
                    }
                }
            }
        }
    }

    // Listener for treeviews, to show properties of selected elements.
    private void selectedElementListener(TreeItem<AcceleratorNode> item) {
        upperPlot.removeAllVerticalRangeMarkers();
        bottomPlot.removeAllVerticalRangeMarkers();

        if (item != null) {
            AcceleratorNode node = item.getValue();

            AttributesTableWrapper attributes = new AttributesTableWrapper(node, getDocument());
            attributes.setTableView(attributesTV, tcName, tcDesignValue, tcLiveValue, tcSimValue);

            EpicsPropertiesTableWrapper epcis = new EpicsPropertiesTableWrapper(node);
            epcis.setTableView(epicsTV, tcHandle, tcEpicsValue);

            ElementPropertiesTableWrapper properties = new ElementPropertiesTableWrapper(node);
            properties.setTableView(propertiesTV, tcProperty, tcValue);

            Accelerator accelerator = getDocument().getAccelerator();
            String rootSeqId = getDocument().getSequence();

            AcceleratorSeq rootSeq = accelerator.getSequence(rootSeqId);

            if (rootSeq == null) {
                rootSeq = accelerator.getComboSequence(rootSeqId);
            }

            // For full accelerator, parent is accelerator.
            if (rootSeq == null) {
                rootSeq = accelerator;
            }

            if (!rootSeq.getAllInclusiveNodes().contains(node)) {
                attributesTV.getItems().clear();
                propertiesTV.getItems().clear();
                epicsTV.getItems().clear();
                return;
            }

            // Add vertical markers to the plot if there is something plotted
            if (!getDocument().getSimulations().isEmpty()) {
                for (TreeItem<AcceleratorNode> selectedItem : modelTreeView.getSelectionModel().getSelectedItems()) {
                    AcceleratorNode selectedNode = selectedItem.getValue();

                    Pair<Number, Number> verticalRangeMarker;
                    if (selectedNode instanceof AcceleratorSeq) {
                        verticalRangeMarker = new Pair<>(rootSeq.getPosition(selectedNode), selectedNode.getLength());
                    } else {
                        verticalRangeMarker = new Pair<>(rootSeq.getPosition(selectedNode) - 0.5 * selectedNode.getLength(), selectedNode.getLength());
                    }

                    upperPlot.addVerticalRangeMarker(verticalRangeMarker);
                    bottomPlot.addVerticalRangeMarker(verticalRangeMarker);
                }
            }
        } else {
            attributesTV.getItems().clear();
            propertiesTV.getItems().clear();
            epicsTV.getItems().clear();
            Simulation sim = getDocument().getSelectedSimulation();
            if (sim != null) {
                sim.clearSimulationListeners();
            }
        }
    }

    @FXML
    private void addSimulation(ActionEvent event) {
        try {
            getDocument().newSimulation();

        } catch (SimulationException ex) {
            Logger.getLogger(FXMLController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        removeB.setDisable(false);
    }

    @FXML
    private void removeSimulation(ActionEvent event) {
        Simulation selectedItem = simTV.getSelectionModel().getSelectedItem();

        // Call stopLiveSimulation method to stop any possible LIVE simulation.
        selectedItem.stopLiveSimulation();

        if (selectedItem != null) {
            simTV.getItems().remove(selectedItem);
        }
        if (simTV.getItems().isEmpty()) {
            removeB.setDisable(true);
        }
    }

    private AcceleratorSimDocument getDocument() {
        return (AcceleratorSimDocument) getApplication().getDocument();
    }
}

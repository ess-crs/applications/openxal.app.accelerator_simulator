/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.accelerator_simulator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.util.Callback;
import xal.ca.Channel;
import xal.ca.ChannelTimeRecord;
import xal.ca.ConnectionListener;
import xal.ca.IEventSinkValTime;
import xal.ca.Monitor;
import xal.ca.MonitorException;
import xal.ca.PutException;
import xal.smf.AcceleratorNode;
import xal.smf.AccessibleProperty;

/**
 * This class wraps around the AccesibleProperties of the
 * {@link xal.smf.AcceleratorNode} object to provide JavaFX beans, which can
 * them be used by JavaFX applications. This class only consider
 * AccessibleProperties that have a corresponding design value in the model.
 * <p>
 * The class also provides a method to represent the properties in a JavaFX
 * TableView.
 * <p>
 * Note: the monitors are cleared when a new AttributesTableWrapper instance is
 * created.
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class AttributesTableWrapper {

    private static final List< PropertyMonitor> monitors = new ArrayList<>();
    protected Simulation sim;
    private final AcceleratorSimDocument document;
    protected final ObservableList<AccessiblePropertyWrapper> accessibleProperties = FXCollections.observableArrayList();
    protected AcceleratorNode node;
    protected static final Map<String, PvSignalsGetter> signalsGetters = new HashMap<>();

    private final Clipboard clipboard = Clipboard.getSystemClipboard();
    private final ClipboardContent content = new ClipboardContent();

    public AttributesTableWrapper(AcceleratorNode node, AcceleratorSimDocument document) {
        this.sim = document.getSelectedSimulation();
        this.node = node;
        this.document = document;
        
        if (sim != null) {
            sim.clearSimulationListeners();
        }

        accessibleProperties.clear();
        signalsGetters.clear();
        clearMonitors();

        addAccessibleProperty(node.getAccessibleProperties());
    }

    protected final void clearMonitors() {
        for (PropertyMonitor monitor : monitors) {
            monitor.clear();
        }
        monitors.clear();
    }

    protected final void addAccessibleProperty(List<AccessibleProperty> properties) {
        for (AccessibleProperty property : properties) {
            if (property.hasDesignValues()) {
                AccessiblePropertyWrapper propertyWrapper = new AccessiblePropertyWrapper(property.getName());

                propertyWrapper.designValueFlagProperty().setValue(true);
                propertyWrapper.designValueProperty().setValue(Double.toString(property.getDesign()));
                propertyWrapper.designValueProperty().addListener((ChangeListener<String>) (ov, t, t1) -> {
                    property.setDesign(Double.parseDouble(t1));
                    document.setHasChanges(true);
                });

                propertyWrapper.liveValueProperty().setValue("NOT CONNECTED");
                propertyWrapper.liveValueProperty().bindBidirectional(propertyWrapper.liveRBValueProperty());

                if (sim != null && sim.getSimAttributes() != null && !sim.getSimAttributes().isEmpty()) {
                    propertyWrapper.simValueProperty().setValue(Double.toString(sim.getSimAttributes().get(node.getId()).get(property.getName())));
                    // Update the value when a new simulation is run
                    sim.addSimulationListener(() -> propertyWrapper.simValueProperty().setValue(Double.toString(sim.getSimAttributes().get(node.getId()).get(property.getName()))));
                }

                accessibleProperties.add(propertyWrapper);

                // Create channel monitor to display and update the live value.
                PropertyMonitor propertyMonitor = new PropertyMonitor(property, propertyWrapper);
                propertyMonitor.start();
                monitors.add(propertyMonitor);
                signalsGetters.put(property.getName(), propertyMonitor);
            }
        }
    }

    public void setTableView(TableView table, TableColumn nameColumn, TableColumn designValueColumn, TableColumn liveValueColumn, TableColumn simValueColumn) {
        table.itemsProperty().setValue(accessibleProperties);
        table.setEditable(true);
        table.getSelectionModel().cellSelectionEnabledProperty().set(true);

        nameColumn.setCellFactory(NonEditableCell.forTableColumn());
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        liveValueColumn.setCellValueFactory(new PropertyValueFactory<>("liveValue"));
        simValueColumn.setCellFactory(NonEditableCell.forTableColumn());
        simValueColumn.setCellValueFactory(new PropertyValueFactory<>("simValue"));

        designValueColumn.setCellValueFactory(new PropertyValueFactory<>("designValue"));
        Callback<TableColumn<AccessiblePropertyWrapper, String>, TableCell<AccessiblePropertyWrapper, String>> designValueClbck = c -> designValueCallback(c);
        designValueColumn.setCellFactory(designValueClbck);

        Callback<TableColumn<AccessiblePropertyWrapper, String>, TableCell<AccessiblePropertyWrapper, String>> clbck = c -> liveValueCallback(c);
        liveValueColumn.setCellFactory(clbck);
    }

    protected TableCell<AccessiblePropertyWrapper, String> liveValueCallback(TableColumn<AccessiblePropertyWrapper, String> column) {
        TableCell<AccessiblePropertyWrapper, String> cell = TextFieldTableCell.<AccessiblePropertyWrapper>forTableColumn().call(column);

        // Stop updating the cell value while editing the cell
        cell.editingProperty().addListener((obs, oldValue, newValue) -> {
            TableRow row = cell.getTableRow();
            if (row != null) {
                AccessiblePropertyWrapper property = (AccessiblePropertyWrapper) row.getItem();
                if (property != null) {
                    if (newValue) {
                        property.liveValueProperty().unbindBidirectional(property.liveRBValueProperty());
                    } else {
                        property.liveValueProperty().bindBidirectional(property.liveRBValueProperty());
                    }
                }
            }
        });

        cell.itemProperty().addListener((obs, oldValue, newValue) -> {
            TableRow row = cell.getTableRow();
            if (row != null) {
                AccessiblePropertyWrapper property = (AccessiblePropertyWrapper) row.getItem();
                if (property != null) {
                    // Set disabled style if channel not connected.
                    ChangeListener<? super Boolean> listener = (ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) -> {
                        if (t1) {
                            cell.setEditable(true);
                            cell.setStyle("");
                        } else {
                            cell.setEditable(false);
                            cell.setStyle(" -fx-font-weight: bold;");
                        }
                    };
                    // Run listener once to apply to current value
                    listener.changed(null, false, property.connectedProperty().getValue());
                    property.connectedProperty().addListener(listener);
                }
            }
        });

        addContextMenu(cell);

        Tooltip tooltip = new Tooltip();
        tooltip.setOnShown((e) -> {
            PvSignalsGetter signalGetter = signalsGetters.get(cell.getTableRow().getItem().getName());
            StringBuilder builder = new StringBuilder();
            builder.append("Readback PV: ");
            builder.append(signalGetter.getReadBackPV());
            builder.append("\nSet PV: ");
            builder.append(signalGetter.getSetPV());
            tooltip.setText(builder.toString());
        });

        cell.tooltipProperty().bind(
                Bindings.when(cell.emptyProperty())
                        .then((Tooltip) null)
                        .otherwise(tooltip));

        return cell;
    }

    protected void addContextMenu(TableCell<AccessiblePropertyWrapper, String> cell) {
        ContextMenu cellMenu = new ContextMenu();
        MenuItem copyRBPvItem = new MenuItem("Copy readback PV to clipboard");
        copyRBPvItem.setOnAction((e) -> {
            PvSignalsGetter signalGetter = signalsGetters.get(cell.getTableRow().getItem().getName());
            content.putString(signalGetter.getReadBackPV());
            clipboard.setContent(content);
        });
        MenuItem copySetPvItem = new MenuItem("Copy set PV to clipboard");
        copySetPvItem.setOnAction((e) -> {
            PvSignalsGetter signalGetter = signalsGetters.get(cell.getTableRow().getItem().getName());
            content.putString(signalGetter.getSetPV());
            clipboard.setContent(content);
        });
        cellMenu.getItems().addAll(copyRBPvItem, copySetPvItem);

        // only display context menu for non-empty cells:
        cell.contextMenuProperty().bind(
                Bindings.when(cell.emptyProperty())
                        .then((ContextMenu) null)
                        .otherwise(cellMenu));
    }

    private TableCell<AccessiblePropertyWrapper, String> designValueCallback(TableColumn<AccessiblePropertyWrapper, String> column) {
        TableCell<AccessiblePropertyWrapper, String> cell = TextFieldTableCell.<AccessiblePropertyWrapper>forTableColumn().call(column);

        // Stop updating the cell value while editing the cell
        cell.editingProperty().addListener((obs, oldValue, newValue) -> {
            TableRow row = cell.getTableRow();
            if (row != null) {
                AccessiblePropertyWrapper property = (AccessiblePropertyWrapper) row.getItem();
                if (property != null) {
                    if (newValue) {
                        property.liveValueProperty().unbindBidirectional(property.liveRBValueProperty());
                    } else {
                        property.liveValueProperty().bindBidirectional(property.liveRBValueProperty());
                    }
                }
            }
        });

        cell.itemProperty().addListener((obs, oldValue, newValue) -> {
            TableRow row = cell.getTableRow();
            if (row != null) {
                AccessiblePropertyWrapper property = (AccessiblePropertyWrapper) row.getItem();
                if (property != null) {
                    // Set disabled style if channel not connected.
                    if (property.hasDesignValue()) {
                        cell.setEditable(true);
                        cell.setStyle("");
                    } else {
                        cell.setEditable(false);
                        cell.setStyle(" -fx-font-weight: bold;");
                    }
                }
            }
        });

        return cell;
    }

    protected interface PvSignalsGetter {

        public String getReadBackPV();

        public String getSetPV();
    }

    /**
     * Properties in this case are always doubles.
     */
    protected class PropertyMonitor implements IEventSinkValTime, ConnectionListener, ChangeListener<String>, PvSignalsGetter {

        AccessiblePropertyWrapper propertyWrapper;
        AccessibleProperty property;
        Channel readBackChannel = null;
        Channel setChannel;
        final Object lock = new Object();
        double lastValue;
        private Monitor monitor;

        PropertyMonitor(AccessibleProperty property, AccessiblePropertyWrapper propertyWrapper) {
            this.propertyWrapper = propertyWrapper;
            this.property = property;
        }

        public synchronized void start() {
            // New values will be sent to the set channel
            setChannel = node.findChannel(property.getSetHandle());

            // The textfield will display the value of the readback channel
            if (property.getReadbackHandles() != null && property.getReadbackHandles().length > 0) {
                readBackChannel = node.findChannel(property.getReadbackHandles()[0]);
            }
            if (readBackChannel == null || setChannel == null) {
                propertyWrapper.liveRBValueProperty().setValue("-");
                return;
            }
            setChannel.requestConnection();

            if (readBackChannel.isConnected()) {
                connectionMade(readBackChannel);
            }
            readBackChannel.addConnectionListener(this);
            readBackChannel.requestConnection();
        }

        public synchronized void clear() {
            if (readBackChannel != null) {
                readBackChannel.removeConnectionListener(this);
            }

            if (monitor != null) {
                try {
                    propertyWrapper.liveRBValueProperty().removeListener(this);
                    monitor.clear();
                    monitor = null;
                } catch (NullPointerException ex) {
                    Logger.getLogger(getClass().getName()).log(Level.FINE, null, ex);
                }
            }
        }

        @Override
        public void eventValue(ChannelTimeRecord ctr, Channel chnl) {
            synchronized (lock) {
                lastValue = property.getLive(new double[]{ctr.doubleValue()});
            }
            propertyWrapper.liveRBValueProperty().setValue(String.valueOf(property.getLive(new double[]{ctr.doubleValue()})));
        }

        @Override
        public synchronized void connectionMade(Channel chnl) {
            // To discard more connectionMade events that may come later
            if (monitor != null && monitor.getChannel() == chnl) {
                return;
            }

            try {
                Logger.getLogger(AttributesTableWrapper.class.getName()).log(Level.INFO, "Connection made, starting monitor for PV {0}", chnl.channelName());
                monitor = chnl.addMonitorValTime(this, 0);
                propertyWrapper.liveRBValueProperty().addListener(this);
                propertyWrapper.connectedProperty().set(true);
            } catch (MonitorException ex) {
                Logger.getLogger(AttributesTableWrapper.class.getName()).log(Level.SEVERE, "Error while starting monitor for PV " + chnl.channelName(), ex);
            }
        }

        @Override
        public synchronized void connectionDropped(Channel chnl) {
            Logger.getLogger(AttributesTableWrapper.class.getName()).log(Level.INFO, "Connection dropped for PV {0}", chnl.channelName());
            propertyWrapper.liveRBValueProperty().removeListener(this);
            propertyWrapper.liveRBValueProperty().setValue("DISCONNECTED");
            propertyWrapper.connectedProperty().set(false);
        }

        @Override
        public void changed(ObservableValue<? extends String> ov, String t, String t1) {
            synchronized (lock) {
                if (!String.valueOf(lastValue).equals(t1)) {
                    try {
                        setChannel.connectAndWait();
                        setChannel.putVal(property.setLive(Double.parseDouble(t1)));
                    } catch (PutException ex) {
                        Logger.getLogger(EpicsPropertiesTableWrapper.class.getName()).log(Level.SEVERE, "PutException in channel " + setChannel.channelName() + " while setting value " + t1 + ". Previous value was " + t + ".", ex);
                    }
                }
            }
        }

        @Override
        public String getReadBackPV() {
            if (readBackChannel != null) {
                return readBackChannel.channelName();
            } else {
                return "";
            }
        }

        @Override
        public String getSetPV() {
            if (setChannel != null) {
                return setChannel.channelName();
            } else {
                return "";
            }
        }
    }
}

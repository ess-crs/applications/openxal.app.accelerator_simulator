/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.accelerator_simulator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.util.Callback;
import xal.ca.Channel;
import xal.ca.ChannelTimeRecord;
import xal.ca.ConnectionException;
import xal.ca.ConnectionListener;
import xal.ca.IEventSinkValTime;
import xal.ca.Monitor;
import xal.ca.MonitorException;
import xal.ca.PutException;
import xal.smf.AcceleratorNode;
import xal.smf.AccessibleProperty;
import xal.tools.ArrayValue;

/**
 * This class wraps around the AccesibleProperties of the
 * {@link xal.smf.AcceleratorNode} object to provide JavaFX beans, which can
 * them be used by JavaFX applications. This class only consider
 * AccessibleProperties that don't have a corresponding design value in the
 * model.
 * <p>
 * The class also provides a method to represent the properties in a JavaFX
 * TableView.
 * <p>
 * Note: the monitors are cleared when a new EpicsPropertiesTableWrapper
 * instance is created.
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class EpicsPropertiesTableWrapper {

    protected final ObservableList<EpicsPropertyWrapper> epicsProperties = FXCollections.observableArrayList();
    private static final List<SignalMonitor> monitors = new ArrayList<>();
    protected static final Map<String, String> pvSignals = new HashMap<>();
    protected AcceleratorNode node;

    private final Clipboard clipboard = Clipboard.getSystemClipboard();
    private final ClipboardContent content = new ClipboardContent();

    public EpicsPropertiesTableWrapper(AcceleratorNode node) {
        this.node = node;
        epicsProperties.clear();
        pvSignals.clear();

        clearMonitors();

        addAccessibleProperty(node.getAccessibleProperties());
    }

    protected final void clearMonitors() {
        for (SignalMonitor monitor : monitors) {
            monitor.clear();
        }
        monitors.clear();
    }

    protected final void addAccessibleProperty(List<AccessibleProperty> properties) {
        for (AccessibleProperty property : properties) {
            if (property.hasLiveValues()) {
                addHandle(property.getSetHandle());
                if (property.getReadbackHandles() != null && property.getReadbackHandles().length > 0) {
                    addHandle(property.getReadbackHandles()[0]);
                }
            }
        }
    }

    private void addHandle(String handle) {
        // Skip duplicated handles
        if (pvSignals.containsKey(handle)) {
            return;
        }

        EpicsPropertyWrapper propertyWrapper = new EpicsPropertyWrapper(handle);
        propertyWrapper.valueProperty().setValue("NOT CONNECTED");
        propertyWrapper.valueProperty().bindBidirectional(propertyWrapper.valueMonitorProperty());

        epicsProperties.add(propertyWrapper);

        // Create channel monitor to display and update the live value.
        SignalMonitor signalMonitor = new SignalMonitor(node, handle, propertyWrapper);
        signalMonitor.start();
        monitors.add(signalMonitor);
        pvSignals.put(handle, signalMonitor.getPV());
    }

    public void setTableView(TableView table, TableColumn nameColumn, TableColumn valueColumn) {
        table.itemsProperty().setValue(epicsProperties);
        table.setEditable(true);
        table.getSelectionModel().cellSelectionEnabledProperty().set(true);

        nameColumn.setCellFactory(NonEditableCell.forTableColumn());
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        valueColumn.setCellValueFactory(new PropertyValueFactory<>("value"));

        Callback<TableColumn<EpicsPropertyWrapper, String>, TableCell<EpicsPropertyWrapper, String>> clbck = c -> valueCallback(c);
        valueColumn.setCellFactory(clbck);
    }

    protected TableCell<EpicsPropertyWrapper, String> valueCallback(TableColumn<EpicsPropertyWrapper, String> column) {
        TableCell<EpicsPropertyWrapper, String> cell = TextFieldTableCell.<EpicsPropertyWrapper>forTableColumn().call(column);

        // Stop updating the cell value while editing the cell
        cell.editingProperty().addListener((obs, oldValue, newValue) -> {
            TableRow row = cell.getTableRow();
            if (row != null) {
                EpicsPropertyWrapper property = (EpicsPropertyWrapper) row.getItem();
                if (property != null) {
                    if (newValue) {
                        property.valueProperty().unbindBidirectional(property.valueMonitorProperty());
                    } else {
                        property.valueProperty().bindBidirectional(property.valueMonitorProperty());
                    }
                }
            }
        });

        cell.itemProperty().addListener((obs, oldValue, newValue) -> {
            TableRow row = cell.getTableRow();
            if (row != null) {
                EpicsPropertyWrapper property = (EpicsPropertyWrapper) row.getItem();
                if (property != null) {
                    // Set disabled style if channel not connected.
                    ChangeListener<? super Boolean> listener = (ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) -> {
                        if (t1) {
                            cell.setEditable(true);
                            cell.setStyle("");
                        } else {
                            cell.setEditable(false);
                            cell.setStyle(" -fx-font-weight: bold;");
                        }
                    };
                    // Run listener once to apply to current value
                    listener.changed(null, false, property.connectedProperty().getValue());
                    property.connectedProperty().addListener(listener);
                }
            }
        });

        addContextMenu(cell);

        Tooltip tooltip = new Tooltip();
        tooltip.setOnShown((e) -> {
            String pvSignal = pvSignals.get(cell.getTableRow().getItem().getName());
            StringBuilder builder = new StringBuilder();
            builder.append("PV: ");
            builder.append(pvSignal);
            tooltip.setText(builder.toString());
        });

        cell.tooltipProperty().bind(
                Bindings.when(cell.emptyProperty())
                        .then((Tooltip) null)
                        .otherwise(tooltip));

        return cell;
    }

    protected void addContextMenu(TableCell<EpicsPropertyWrapper, String> cell) {
        ContextMenu cellMenu = new ContextMenu();
        MenuItem copyPvItem = new MenuItem("Copy PV to clipboard");
        copyPvItem.setOnAction((e) -> {
            String pvName = pvSignals.get(cell.getTableRow().getItem().getName());
            content.putString(pvName);
            clipboard.setContent(content);
        });
        cellMenu.getItems().addAll(copyPvItem);

        // only display context menu for non-empty cells:
        cell.contextMenuProperty().bind(
                Bindings.when(cell.emptyProperty())
                        .then((ContextMenu) null)
                        .otherwise(cellMenu));
    }

    protected interface ObjectGetter {

        public Object get(ChannelTimeRecord ctr);
    }

    protected interface ObjectSetter {

        public void put(String string) throws ConnectionException, PutException;
    }

    protected class SignalMonitor implements IEventSinkValTime, ConnectionListener, ChangeListener<String> {

        EpicsPropertyWrapper propertyWrapper;
        Channel channel;
        String handle;
        Class<?> dataType;
        final Object lock = new Object();
        Object lastValue;
        Map<Class<?>, ObjectGetter> getObjectMap;
        Map<Class<?>, ObjectSetter> putObjectMap;
        private Monitor monitor;

        SignalMonitor(AcceleratorNode node, String handle, EpicsPropertyWrapper propertyWrapper) {
            this.propertyWrapper = propertyWrapper;
            this.handle = handle;
        }

        public String getPV() {
            if (channel != null) {
                return channel.channelName();
            } else {
                return "";
            }
        }

        public void start() {
            // New values will be sent to the PV channel
            channel = node.findChannel(handle);

            if (channel == null) {
                propertyWrapper.valueProperty().setValue("-");
                return;
            }

            // Map to get any type of data and store it as Object.
            getObjectMap = new HashMap<>();
            getObjectMap.put(String.class, (ChannelTimeRecord ctr) -> ctr.stringValue());
            getObjectMap.put(byte.class, (ChannelTimeRecord ctr) -> ctr.byteValue());
            getObjectMap.put(short.class, (ChannelTimeRecord ctr) -> ctr.shortValue());
            getObjectMap.put(int.class, (ChannelTimeRecord ctr) -> ctr.intValue());
            getObjectMap.put(long.class, (ChannelTimeRecord ctr) -> ctr.longValue());
            getObjectMap.put(float.class, (ChannelTimeRecord ctr) -> ctr.floatValue());
            getObjectMap.put(double.class, (ChannelTimeRecord ctr) -> ctr.doubleValue());
            getObjectMap.put(String[].class, (ChannelTimeRecord ctr) -> ctr.stringArray());
            getObjectMap.put(byte[].class, (ChannelTimeRecord ctr) -> ctr.byteArray());
            getObjectMap.put(short[].class, (ChannelTimeRecord ctr) -> ctr.shortArray());
            getObjectMap.put(int[].class, (ChannelTimeRecord ctr) -> ctr.intArray());
            getObjectMap.put(long[].class, (ChannelTimeRecord ctr) -> ctr.longArray());
            getObjectMap.put(float[].class, (ChannelTimeRecord ctr) -> ctr.floatArray());
            getObjectMap.put(double[].class, (ChannelTimeRecord ctr) -> ctr.doubleArray());
            // Map to put any type of data from a String.
            putObjectMap = new HashMap<>();
            putObjectMap.put(String.class, (String string) -> channel.putVal(string));
            putObjectMap.put(byte.class, (String string) -> channel.putVal(Byte.parseByte(string)));
            putObjectMap.put(short.class, (String string) -> channel.putVal(Short.parseShort(string)));
            putObjectMap.put(int.class, (String string) -> channel.putVal(Integer.parseInt(string)));
            putObjectMap.put(long.class, (String string) -> channel.putVal(Long.parseLong(string)));
            putObjectMap.put(float.class, (String string) -> channel.putVal(Float.parseFloat(string)));
            putObjectMap.put(double.class, (String string) -> channel.putVal(Double.parseDouble(string)));
            putObjectMap.put(String[].class, (String string) -> channel.putVal(toStringArray(string)));
            putObjectMap.put(byte[].class, (String string) -> channel.putVal(toStringStore(string).byteArray()));
            putObjectMap.put(short[].class, (String string) -> channel.putVal(toStringStore(string).shortArray()));
            putObjectMap.put(int[].class, (String string) -> channel.putVal(toStringStore(string).intArray()));
            putObjectMap.put(long[].class, (String string) -> channel.putVal(toStringStore(string).longArray()));
            putObjectMap.put(float[].class, (String string) -> channel.putVal(toStringStore(string).floatArray()));
            putObjectMap.put(double[].class, (String string) -> channel.putVal(toStringStore(string).doubleArray()));

            channel.addConnectionListener(this);
            channel.requestConnection();
        }

        private String[] toStringArray(String string) {
            return string.substring(1, string.length() - 1).split(", ");
        }

        private ArrayValue toStringStore(String string) {
            return ArrayValue.stringStore(toStringArray(string));
        }

        public void clear() {
            if (channel != null) {
                channel.removeConnectionListener(this);
            }

            if (monitor != null) {
                try {
                    propertyWrapper.valueProperty().removeListener(this);
                    monitor.clear();
                } catch (Exception ex) {
                    Logger.getLogger(getClass().getName()).log(Level.FINE, null, ex);
                }
            }
        }

        @Override
        public void eventValue(ChannelTimeRecord ctr, Channel chnl) {
            dataType = ctr.getType();
            synchronized (lock) {
                lastValue = getObjectMap.get(dataType).get(ctr);
            }
            propertyWrapper.valueMonitorProperty().setValue(ctr.stringValue());
        }

        @Override
        public void connectionMade(Channel chnl) {
            // To discard more connectionMade events that may come later
            if (monitor != null && monitor.getChannel() == chnl) {
                return;
            }

            try {
                Logger.getLogger(EpicsPropertiesTableWrapper.class.getName()).log(Level.INFO, "Connection made, starting monitor for PV {0}", chnl.channelName());
                monitor = chnl.addMonitorValTime(this, 0);
                propertyWrapper.valueProperty().addListener(this);
                propertyWrapper.connectedProperty().set(true);
            } catch (MonitorException ex) {
                Logger.getLogger(EpicsPropertiesTableWrapper.class.getName()).log(Level.SEVERE, "Error while starting monitor for PV " + chnl.channelName(), ex);
            } catch (NullPointerException ex) {
                Logger.getLogger(EpicsPropertiesTableWrapper.class.getName()).log(Level.SEVERE, "channel = " + chnl.channelName() + " ", ex);
                chnl.disconnect();
                chnl.requestConnection();
            }
        }

        @Override
        public void connectionDropped(Channel chnl) {
            Logger.getLogger(EpicsPropertiesTableWrapper.class.getName()).log(Level.INFO, "Connection dropped for PV {0}", chnl.channelName());
            propertyWrapper.valueProperty().removeListener(this);
            propertyWrapper.valueProperty().setValue("DISCONNECTED");
            propertyWrapper.connectedProperty().set(false);
        }

        @Override
        public void changed(ObservableValue<? extends String> ov, String t, String t1) {
            synchronized (lock) {
                if (!lastValue.toString().equals(t1)) {
                    try {
                        channel.connectAndWait();
                        putObjectMap.get(dataType).put(t1);
                    } catch (ConnectionException | PutException ex) {
                        Logger.getLogger(EpicsPropertiesTableWrapper.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }
}

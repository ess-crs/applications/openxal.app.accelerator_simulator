/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.accelerator_simulator;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import xal.app.accelerator_simulator.plot.PlotType;
import xal.extension.fxapplication.XalFxDocument;
import xal.smf.AcceleratorSeq;
import xal.tools.data.DataAdaptor;
import xal.tools.xml.XmlDataAdaptor;

/**
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class AcceleratorSimDocument extends XalFxDocument {

    private ObservableList<Simulation> simulations = FXCollections.observableArrayList();
    private Simulation selectedSimulation;
    private static final String DOCUMENT_VERSION = "1.0";

    // Default values
    private static final boolean PLOT_DIAG_FLAG_DEF = true;
    private static final boolean PLOT_APER_FLAG_DEF = true;
    private static final String UPPER_PLOT_TYPE_DEF = PlotType.TRAJECTORY.toString();
    private static final String BOTTOM_PLOT_TYPE_DEF = PlotType.ENVELOPE.toString();

    private BooleanProperty plotDiagnosticsFlag = new SimpleBooleanProperty(PLOT_DIAG_FLAG_DEF);
    private BooleanProperty plotApertureFlag = new SimpleBooleanProperty(PLOT_APER_FLAG_DEF);

    private StringProperty upperPlotType = new SimpleStringProperty(UPPER_PLOT_TYPE_DEF);
    private StringProperty bottomPlotType = new SimpleStringProperty(BOTTOM_PLOT_TYPE_DEF);

    AcceleratorSimDocument(Stage stage) {
        super(stage);

        DEFAULT_FILENAME = "sim.asd";
        FILETYPE_DESCRIPTION = "Accelerator Simulator Document File";
        WILDCARD_FILE_EXTENSION = "*.asd";
//        HELP_PAGEID = "";
    }

    public ObservableList<Simulation> getSimulations() {
        return simulations;
    }

    public BooleanProperty plotDiagnosticsFlag() {
        return plotDiagnosticsFlag;
    }

    public BooleanProperty plotApertureFlag() {
        return plotApertureFlag;
    }

    public boolean isPlottingDiagnostics() {
        return plotDiagnosticsFlag.get();
    }

    public boolean isPlottingAperture() {
        return plotApertureFlag.get();
    }

    public StringProperty upperPlotType() {
        return upperPlotType;
    }

    public StringProperty bottomPlotType() {
        return bottomPlotType;
    }

    public PlotType getUpperPlotType() {
        return PlotType.fromString(upperPlotType.get());
    }

    public PlotType getBottomPlotType() {
        return PlotType.fromString(bottomPlotType.get());
    }

    /**
     * Add a new simulation to the document.
     */
    public Simulation newSimulation() throws SimulationException {
        Simulation sim;
        if (getSequence() != null && !"".equals(getSequence())) {
            AcceleratorSeq seq = getAccelerator().getSequence(getSequence());
            if (seq == null) {
                seq = getAccelerator().getComboSequence(getSequence());
            }
            if (seq == null) {
                throw new SimulationException("The accelerator " + getAccelerator().getId() + " does not contain sequence " + getSequence());
            }
            sim = new Simulation(seq);
            simulations.add(sim);
        } else {
            sim = new Simulation(getAccelerator());
            simulations.add(sim);
        }
        setHasChanges(true);

        return sim;
    }

    /**
     * Remove a simulation from the document.
     *
     * @param sim
     */
    public boolean removeSimulation(Simulation sim) {
        setHasChanges(true);

        return simulations.remove(sim);
    }

    /**
     * Save the ScannerDocument document to the specified URL.
     *
     * @param url The file URL where the data should be saved
     */
    @Override
    public void saveDocumentAs(URL url) {
        // Dump attributes from the currently selected simulation before saving to file.
        selectedSimulation.storeCurrentAttributes();

        XmlDataAdaptor da = XmlDataAdaptor.newEmptyDocumentAdaptor();
        DataAdaptor mainAdaptor = da.createChild("Accelerator_Simulator_Data");
        mainAdaptor.setValue("version", DOCUMENT_VERSION);
        mainAdaptor.setValue("selected", getSimulations().indexOf(selectedSimulation));

        mainAdaptor.setValue("plotDiagnostics", plotDiagnosticsFlag.get());
        mainAdaptor.setValue("plotAperture", plotApertureFlag.get());

        mainAdaptor.setValue("upperPlotType", upperPlotType.get());
        mainAdaptor.setValue("bottomPlotType", bottomPlotType.get());

        for (Simulation sim : getSimulations()) {
            sim.write(mainAdaptor);
        }

        da.writeToUrl(url);

        setHasChanges(false);
    }

    /**
     * Reads the content of the document from the specified URL, and loads the information into the application.
     *
     * @param url The path to the XML file
     */
    @Override
    public void loadDocument(URL url) {
        // TODO: let user choose whether to add more simulations or create new document.
        simulations.clear();
        setHasChanges(false);

        XmlDataAdaptor da = XmlDataAdaptor.adaptorForUrl(url, false);

        DataAdaptor mainAdaptor = da.childAdaptor("Accelerator_Simulator_Data");
        if (mainAdaptor == null || !mainAdaptor.hasAttribute("version")) {
            String msg = "Malformed document";
            Logger.getLogger(AcceleratorSimDocument.class.getName()).log(Level.FINE, "{0}.", msg);
            showErrorDialog(msg, "Please check that the file is an Accelerator Simulator Document.");
            return;
        }

        if (!DOCUMENT_VERSION.equals(mainAdaptor.stringValue("version"))) {
            String msg = "Document version different from expected";
            Logger.getLogger(AcceleratorSimDocument.class.getName()).log(Level.FINE, "{0}.", msg);
            showErrorDialog(msg, "File version is " + mainAdaptor.stringValue("version") + " . Expected " + DOCUMENT_VERSION + " .");
            return;
        }

        List<Simulation> newSimulations = new ArrayList<>();

        for (DataAdaptor simAdaptor : mainAdaptor.childAdaptors("simulation")) {
            Simulation sim = new Simulation();
            try {
                // TODO: let user choose accelerator
                sim.load(simAdaptor, this.getAccelerator());
            } catch (LoadingFileException ex) {
                Logger.getLogger(AcceleratorSimDocument.class.getName()).log(Level.FINE, null, ex);
                showErrorDialog("Error loading file", "One simulation could not be loaded.");
                return;
            }
            newSimulations.add(sim);
        }

        simulations.addAll(newSimulations);

        if (mainAdaptor == null || !mainAdaptor.hasAttribute("selected")) {
            String msg = "Malformed document";
            Logger.getLogger(AcceleratorSimDocument.class.getName()).log(Level.FINE, "{0}.", msg);
            showErrorDialog(msg, "Please check that the file is an Accelerator Simulator Document.");
            return;
        }

        // Trick to restore the selection on simulation table
        int selected = mainAdaptor.intValue("selected");
        simulations.remove(selected);
        simulations.add(selected, newSimulations.get(selected));

        plotDiagnosticsFlag.set(mainAdaptor.booleanValue("plotDiagnostics"));
        plotApertureFlag.set(mainAdaptor.booleanValue("plotAperture"));

        upperPlotType.set(mainAdaptor.stringValue("upperPlotType"));
        bottomPlotType.set(mainAdaptor.stringValue("bottomPlotType"));
    }

    protected static void showErrorDialog(String title, String msg) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(title + ".");
        alert.setContentText(msg);
        alert.showAndWait();
    }

    @Override
    public void newDocument() {
        if (hasChanges()) {
            Alert dialog = new Alert(Alert.AlertType.CONFIRMATION);
            dialog.setHeaderText("Document has unsaved changes, are you sure you want to create a new document?");
            dialog.setContentText("Unsaved changes in the current document will be lost.");
            Optional<ButtonType> result = dialog.showAndWait();
            if (result.isPresent() && result.get() == ButtonType.OK) {
                simulations.clear();
                source = null;
                setHasChanges(false);
                plotDiagnosticsFlag.set(PLOT_DIAG_FLAG_DEF);
                plotApertureFlag.set(PLOT_APER_FLAG_DEF);
                upperPlotType.set(UPPER_PLOT_TYPE_DEF);
                bottomPlotType.set(BOTTOM_PLOT_TYPE_DEF);
            }
        }
    }

    protected void setSelectedSimulation(Simulation newSim) {
        selectedSimulation = newSim;
    }

    protected Simulation getSelectedSimulation() {
        return selectedSimulation;
    }
}

/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.accelerator_simulator;

import xal.app.accelerator_simulator.plot.ChartWrapper;
import eu.ess.xaos.ui.util.ColorUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.util.Pair;
import xal.ca.BatchConnectionRequest;
import xal.ca.Channel;
import xal.ca.ChannelRecord;
import xal.ca.ConnectionListener;
import xal.ca.IEventSinkValue;
import xal.ca.Monitor;
import xal.ca.MonitorException;
import xal.smf.AcceleratorNode;
import xal.smf.AcceleratorSeq;
import xal.smf.impl.BPM;

/**
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class DiagnosticsMonitor extends AbstractMonitorsHandler {

    protected Map<BPM, BPMData> bpms = new TreeMap<>();

    private List<Pair<Number, Number>> xAvg = new ArrayList<>();
    private List<Pair<Number, Number>> yAvg = new ArrayList<>();

    DiagnosticsMonitor(Simulation sim, EventHandler<ActionEvent> handler) {
        super(sim, handler);
    }

    @Override
    protected void initializeMonitors() {
        bpms.clear();

        List<Channel> channels = new ArrayList<>();

        if (sim != null) {
            AcceleratorSeq sequence = sim.getSequence();

            // Get BPMs
            for (AcceleratorNode node : sequence.getAllNodesOfType("BPM")) {
                BPM bpm = (BPM) node;
                BPMData bpmData = new BPMData(bpm, sequence);
                bpms.put(bpm, bpmData);

                channels.add(bpm.getChannel(BPM.X_AVG_HANDLE));
                channels.add(bpm.getChannel(BPM.Y_AVG_HANDLE));
            }

            BatchConnectionRequest request = new BatchConnectionRequest(channels);
            request.submitAndWait(5.0);

            for (BPMData bpmData : bpms.values()) {
                bpmData.startMonitors();
            }

            // Populate the data for the first time
            updateData();
        }
    }

    public synchronized List<Pair<Number, Number>> getXAvg() {
        return xAvg;
    }

    public synchronized List<Pair<Number, Number>> getYAvg() {
        return yAvg;
    }

    @Override
    protected void handle() {
        // Update the arrays to be plotted
        if (sim.isPlotted()) {
            updateData();
        }

        // Trigger update event
        handler.handle(null);
    }

    private synchronized void updateData() {
        // Collect all data   
        xAvg.clear();
        yAvg.clear();

        if (!bpms.isEmpty()) {
            for (BPMData bpm : bpms.values()) {
                // BPM reading in um
                xAvg.add(new Pair(bpm.getPosition(), bpm.getPhaseXAvg() * 1e-3));
                yAvg.add(new Pair(bpm.getPosition(), bpm.getPhaseYAvg() * 1e-3));
            }
        }
    }

    public void plot(ChartWrapper chart) {
        if (!xAvg.isEmpty() && !yAvg.isEmpty()) {
            ObservableList<XYChart.Data<Number, Number>> dataX = FXCollections.observableArrayList();
            for (Pair<Number, Number> point : xAvg) {
                dataX.add(new XYChart.Data<>(point.getKey(), point.getValue()));
            }
            ObservableList<XYChart.Data<Number, Number>> dataY = FXCollections.observableArrayList();
            for (Pair<Number, Number> point : yAvg) {
                dataY.add(new XYChart.Data<>(point.getKey(), point.getValue()));
            }

            Series bpmXSeries = chart.addDataSeries("BPM x", dataX);
            Series bpmXYSeries = chart.addDataSeries("BPM y", dataY);

            chart.getChart().setSeriesColor(bpmXSeries, ColorUtils.ESS_SECONDARY);
            chart.getChart().setSeriesColor(bpmXYSeries, ColorUtils.ESS_PRIMARY_LIGHT);

            chart.getChart().setShowMarker(bpmXSeries, true);
            chart.getChart().setShowMarker(bpmXYSeries, true);
            chart.getChart().setShowLine(bpmXSeries, false);
            chart.getChart().setShowLine(bpmXYSeries, false);
        }
    }

    class BPMData implements IEventSinkValue, ConnectionListener {

        private final double position;
        private double xAvg;
        private double yAvg;
        private final Channel channelX;
        private final Channel channelY;

        BPMData(BPM bpm, AcceleratorSeq sequence) {
            this.position = sequence.getPosition(bpm);

            channelX = bpm.getChannel(BPM.X_AVG_HANDLE);
            channelY = bpm.getChannel(BPM.Y_AVG_HANDLE);
        }

        public void startMonitors() {
            channelX.addConnectionListener(this);
            channelY.addConnectionListener(this);
        }

        public void setxAvg(double xAvg) {
            this.xAvg = xAvg;
        }

        public void setyAvg(double yAvg) {
            this.yAvg = yAvg;
        }

        @Override
        public void eventValue(ChannelRecord cr, Channel chnl) {
            boolean newData = false;
            if (chnl.channelName().equals(channelX.channelName())) {
                if (xAvg != cr.doubleValue()) {
                    newData = true;
                }
                xAvg = cr.doubleValue();
            } else if (chnl.channelName().equals(channelY.channelName())) {
                if (yAvg != cr.doubleValue()) {
                    newData = true;
                }
                yAvg = cr.doubleValue();
            }
            if (newData) {
                notifyNewUpdate();
            }
        }

        public double getPosition() {
            return position;
        }

        public double getPhaseXAvg() {
            return xAvg;
        }

        public double getPhaseYAvg() {
            return yAvg;
        }

        @Override
        public void connectionMade(Channel chnl) {
            try {
                if (chnl.isConnected()) {
                    Monitor monitor = chnl.addMonitorValue(this, 0);
                    monitors.add(monitor);
                }
            } catch (MonitorException ex) {
                Logger.getLogger(LiveSimulationRunner.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NullPointerException ex) {
                Logger.getLogger(LiveSimulationRunner.class.getName()).log(Level.WARNING, "Not possible to connect to channel {0}.", chnl.channelName());
                Logger.getLogger(LiveSimulationRunner.class.getName()).log(Level.FINE, "NullPointerException while connecting to channel " + chnl.channelName() + ".", ex);
                chnl.disconnect();
                chnl.requestConnection();
            }
        }

        @Override
        public void connectionDropped(Channel chnl) {
            Logger.getLogger(LiveSimulationRunner.class.getName()).log(Level.WARNING, "Channel {0} disconnected.", chnl.channelName());
        }
    }
}

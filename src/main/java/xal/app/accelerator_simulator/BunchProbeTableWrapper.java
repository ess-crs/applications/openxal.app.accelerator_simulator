/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.accelerator_simulator;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import xal.model.probe.BunchProbe;

/**
 * This class wraps around the properties of the
 * {@link xal.model.probe.BunchProbe} object to provide JavaFX beans, which can
 * them be used by JavaFX applications.
 * <p>
 * The class also provides a method to represent the properties in a JavaFX
 * TableView.
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class BunchProbeTableWrapper {

    private final ObservableList<PropertyWrapper> properties = FXCollections.observableArrayList();

    /**
     *
     * @param node
     */
    public BunchProbeTableWrapper(BunchProbe probe) {
        PropertyWrapper currentProperty = new PropertyWrapper("Beam current [mA]", probe.getBeamCurrent());
        PropertyWrapper betaProperty = new PropertyWrapper("Relativistic beta", Double.toString(probe.getBeta()));
        PropertyWrapper gammaProperty = new PropertyWrapper("Relativistic gamma", Double.toString(probe.getGamma()));
        PropertyWrapper kEnergyProperty = new PropertyWrapper("Kinetic Energy [MeV]", Double.toString(probe.getKineticEnergy()));
        PropertyWrapper speciesProperty = new PropertyWrapper("Species", probe.getSpeciesName());
        PropertyWrapper restMassProperty = new PropertyWrapper("Rest mass [MeV]", probe.getSpeciesRestEnergy());
        PropertyWrapper chargeProperty = new PropertyWrapper("Charge [e]", probe.getSpeciesCharge());

        properties.addAll(currentProperty, betaProperty, gammaProperty, kEnergyProperty, speciesProperty, restMassProperty, chargeProperty);

        // Bind properties to AcceleratorNode fields.
        currentProperty.valueProperty().addListener((ChangeListener<String>) (ObservableValue<? extends String> ov, String t, String t1) -> probe.setBeamCurrent(Double.parseDouble(t1)));
        betaProperty.valueProperty().addListener((ChangeListener<String>) (ObservableValue<? extends String> ov, String t, String t1) -> {
            double beta = Double.parseDouble(t1);
            double gamma = 1.0 / Math.sqrt(1.0 - beta * beta);
            probe.setKineticEnergy(gamma * probe.getSpeciesRestEnergy());
        });
        gammaProperty.valueProperty().addListener((ChangeListener<String>) (ObservableValue<? extends String> ov, String t, String t1) -> {
            double gamma = Double.parseDouble(t1);
            probe.setKineticEnergy(gamma * probe.getSpeciesRestEnergy());
        });
        kEnergyProperty.valueProperty().addListener((ChangeListener<String>) (ObservableValue<? extends String> ov, String t, String t1) -> probe.setKineticEnergy(Double.parseDouble(t1)));
        speciesProperty.valueProperty().addListener((ChangeListener<String>) (ObservableValue<? extends String> ov, String t, String t1) -> probe.setSpeciesName(t1));
        restMassProperty.valueProperty().addListener((ChangeListener<String>) (ObservableValue<? extends String> ov, String t, String t1) -> probe.setSpeciesRestEnergy(Double.parseDouble(t1)));
        chargeProperty.valueProperty().addListener((ChangeListener<String>) (ObservableValue<? extends String> ov, String t, String t1) -> probe.setSpeciesCharge(Double.parseDouble(t1)));
    }

    public void setTableView(TableView table, TableColumn nameColumn, TableColumn valueColumn) {
        table.setEditable(true);
        table.getSelectionModel().cellSelectionEnabledProperty().set(true);

        nameColumn.setCellFactory(NonEditableCell.forTableColumn());
        nameColumn.setCellFactory(TextFieldTableCell.forTableColumn());

        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        valueColumn.setCellValueFactory(new PropertyValueFactory<>("value"));

        table.itemsProperty().setValue(properties);
    }
}

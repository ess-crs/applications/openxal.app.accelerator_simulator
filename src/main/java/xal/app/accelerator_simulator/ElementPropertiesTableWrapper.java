/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.accelerator_simulator;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import xal.smf.AcceleratorNode;
import xal.smf.impl.Electromagnet;

/**
 * This class wraps around the main properties of the
 * {@link xal.smf.AcceleratorNode} object to provide JavaFX beans, which can
 * them be used by JavaFX applications.
 * <p>
 * The class also provides a method to represent the properties in a JavaFX
 * TableView.
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class ElementPropertiesTableWrapper {

    private final ObservableList<PropertyWrapper> properties = FXCollections.observableArrayList();

    /**
     *
     * @param node
     */
    public ElementPropertiesTableWrapper(AcceleratorNode node) {
        PropertyWrapper nodeIdProperty = new PropertyWrapper("Node ID", node.getId());
        PropertyWrapper positionProperty = new PropertyWrapper("Position", Double.toString(node.getPosition()));
        PropertyWrapper lengthProperty = new PropertyWrapper("Length", Double.toString(node.getLength()));
        PropertyWrapper sDisplayProperty = new PropertyWrapper("SDisplay", Double.toString(node.getSDisplay()));
        PropertyWrapper typeProperty = new PropertyWrapper("Type", node.getType());
        PropertyWrapper softTypeProperty = new PropertyWrapper("SoftType", node.getSoftType());
        PropertyWrapper eIdProperty = new PropertyWrapper("Engineering ID", node.getEId());
        PropertyWrapper pIdProperty = new PropertyWrapper("Physics ID", node.getPId());
        PropertyWrapper statusProperty = new PropertyWrapper("Status", (Boolean) node.getStatus());
        PropertyWrapper validProperty = new PropertyWrapper("Valid", (Boolean) node.getValid());

        properties.addAll(nodeIdProperty, positionProperty, lengthProperty, sDisplayProperty, typeProperty, softTypeProperty, eIdProperty, pIdProperty, statusProperty, validProperty);

        if (node instanceof Electromagnet) {
            PropertyWrapper powerSupplyProperty = new PropertyWrapper("PowerSupply", ((Electromagnet) node).getMainSupply().getId());
            properties.add(powerSupplyProperty);
        }
    }

    public void setTableView(TableView table, TableColumn nameColumn, TableColumn valueColumn) {
        table.setEditable(true);
        table.getSelectionModel().cellSelectionEnabledProperty().set(true);

        nameColumn.setCellFactory(NonEditableCell.forTableColumn());

        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        valueColumn.setCellValueFactory(new PropertyValueFactory<>("value"));

        table.itemsProperty().setValue(properties);
    }
}

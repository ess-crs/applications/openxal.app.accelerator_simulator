/*
 * Copyright (C) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.accelerator_simulator;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This is a wrapper around a PV and its value to create a JavaFX bean.
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class EpicsPropertyWrapper {

    private final StringProperty name = new SimpleStringProperty("–");
    private final BooleanProperty connected = new SimpleBooleanProperty(false);
    private final StringProperty value = new SimpleStringProperty("-");
    private final StringProperty valueMonitor = new SimpleStringProperty("-");

    EpicsPropertyWrapper(String name) {
        this.name.setValue(name);
        this.value.setValue("");
    }

    public String getName() {
        return name.getValue();
    }

    public String getValue() {
        return value.getValue();
    }

    public boolean getConnected() {
        return connected.getValue();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public StringProperty valueProperty() {
        return value;
    }

    public StringProperty valueMonitorProperty() {
        return valueMonitor;
    }

    public BooleanProperty connectedProperty() {
        return connected;
    }
}

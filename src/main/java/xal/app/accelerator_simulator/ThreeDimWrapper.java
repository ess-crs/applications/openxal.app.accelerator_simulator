/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.accelerator_simulator;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class ThreeDimWrapper {

    private StringProperty name = new SimpleStringProperty("–");
    private StringProperty valueX = new SimpleStringProperty("–");
    private StringProperty valueY = new SimpleStringProperty("–");
    private StringProperty valueZ = new SimpleStringProperty("–");

    ThreeDimWrapper(String name, double[] value) {
        this.name.set(name);
        valueX.set(Double.toString(value[0]));
        valueY.set(Double.toString(value[1]));
        valueZ.set(Double.toString(value[2]));
    }

    public String getName() {
        return name.get();
    }

    public double getValueX() {
        return Double.parseDouble(valueX.get());
    }

    public double getValueY() {
        return Double.parseDouble(valueY.get());
    }

    public double getValueZ() {
        return Double.parseDouble(valueZ.get());
    }

    public StringProperty nameProperty() {
        return name;
    }

    public StringProperty valueXProperty() {
        return valueX;
    }

    public StringProperty valueYProperty() {
        return valueY;
    }

    public StringProperty valueZProperty() {
        return valueZ;
    }

    public double[] getValue() {
        double[] value = new double[3];
        value[0] = Double.parseDouble(valueX.get());
        value[1] = Double.parseDouble(valueY.get());
        value[2] = Double.parseDouble(valueZ.get());

        return value;
    }

    public void setValue(double[] value) {
        valueX.set(Double.toString(value[0]));
        valueY.set(Double.toString(value[1]));
        valueZ.set(Double.toString(value[2]));
    }
}

/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.accelerator_simulator;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class PropertyWrapper {

    private StringProperty name = new SimpleStringProperty("–");
//    private StringProperty value = new SimpleStringProperty("–");
    private ObjectProperty value = new SimpleObjectProperty("–");
    private StringProperty tooltip = new SimpleStringProperty("–");

    PropertyWrapper(String name, Object value) {
        this(name, value, name);
    }

    PropertyWrapper(String name, Object value, String tooltip) {
        this.name.setValue(name);
        if (value != null) {
            this.value.setValue(value);
        }
        this.tooltip.setValue(tooltip);
    }

    public String getName() {
        return name.get();
    }

    public Object getValue() {
        return value.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public ObjectProperty valueProperty() {
        return value;
    }

    public StringProperty tooltipProperty() {
        return tooltip;
    }

    public void setValue(Object current) {
        value.set(current);
    }
}
